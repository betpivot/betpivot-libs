package com.betting.storage.config;

import com.mongodb.MongoClient;
import com.mongodb.MongoClientURI;
import org.mongodb.morphia.Datastore;
import org.mongodb.morphia.Morphia;
import org.mongodb.morphia.annotations.Entity;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.beans.factory.config.BeanDefinition;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ClassPathScanningCandidateComponentProvider;
import org.springframework.context.annotation.Configuration;
import org.springframework.core.type.filter.AnnotationTypeFilter;

//@Configuration
//@ConfigurationProperties
public class SeparateDatastoresDatastoreMorphiaAutoConfiguration {

    @Bean(name = "bettingDatastore")
    public Datastore bettingDatastore(
            @Value("${mongo.morphia.betting.db-name}") String dbName,
            @Value("${mongo.morphia.betting.connection-string}") String connectionString) throws ClassNotFoundException {
        return getDatastore(dbName, "com.betting.storage.data.betting",connectionString);
    }

    @Bean(name = "predictionDatastore")
    public Datastore predictionDatastore(
            @Value("${mongo.morphia.prediction.db-name}") String dbName,
            @Value("${mongo.morphia.prediction.connection-string}") String connectionString) throws ClassNotFoundException {
        return getDatastore(dbName, "com.betting.storage.data.prediction", connectionString);
    }

    @Bean(name = "configurationDatastore")
    public Datastore configurationDatastore(
            @Value("${mongo.morphia.configuration.db-name}") String dbName,
            @Value("${mongo.morphia.configuration.connection-string}") String connectionString) throws ClassNotFoundException {
        return getDatastore(dbName, "com.betting.storage.data.config", connectionString);
    }

    @Bean(name = "suggestionDatastore")
    public Datastore suggestionDatastore(
            @Value("${mongo.morphia.suggestion.db-name}") String dbName,
            @Value("${mongo.morphia.suggestion.connection-string}") String connectionString) throws ClassNotFoundException {
        return getDatastore(dbName, "com.betting.storage.data.suggestion", connectionString);
    }

    @Bean(name = "resultDatastore")
    public Datastore resultDatastore(
            @Value("${mongo.morphia.result.db-name}") String dbName,
            @Value("${mongo.morphia.result.connection-string}") String connectionString) throws ClassNotFoundException {
        return getDatastore(dbName, "com.betting.storage.data.result", connectionString);
    }

    @Bean(name = "performanceDatastore")
    public Datastore performanceDatastore(
            @Value("${mongo.morphia.performance.db-name}") String dbName,
            @Value("${mongo.morphia.performance.connection-string}") String connectionString) throws ClassNotFoundException {
        return getDatastore(dbName, "com.betting.storage.data.performance", connectionString);
    }

    private Datastore getDatastore(String dbName, String pckg, String connectionString) throws ClassNotFoundException {
        Morphia morphia = new Morphia();
        ClassPathScanningCandidateComponentProvider entityScanner = new ClassPathScanningCandidateComponentProvider(true);
        entityScanner.addIncludeFilter(new AnnotationTypeFilter(Entity.class));
        for (BeanDefinition candidate: entityScanner.findCandidateComponents(pckg)) {
            morphia.map(Class.forName(candidate.getBeanClassName()));
        }

        Datastore datastore = morphia.createDatastore(new MongoClient(new MongoClientURI(connectionString)), dbName);
        datastore.ensureIndexes();
        return datastore;
    }

}
