package com.betting.storage.data.messaging;

import lombok.Data;
import lombok.NoArgsConstructor;
import org.bson.types.ObjectId;
import org.mongodb.morphia.annotations.Entity;
import org.mongodb.morphia.annotations.Id;

@Entity
@Data
@NoArgsConstructor
public class BettingMessage {

    @Id
    private ObjectId id;

    private BettingMessageType type;

    private String detail;

    public BettingMessage(BettingMessageType type, String detail) {
        this.type = type;
        this.detail = detail;
    }
}
