package com.betting.storage.data.messaging;

public enum BettingMessageType {
    NEW_SUGGESTION,
    NEW_PREDICTION;
}
