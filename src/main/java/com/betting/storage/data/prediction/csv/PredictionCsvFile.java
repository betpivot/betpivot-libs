package com.betting.storage.data.prediction.csv;

import com.betting.storage.data.prediction.Prediction;
import com.betting.storage.data.prediction.PredictionMatch;
import com.betting.storage.data.prediction.PredictionPlayerName;
import com.betting.storage.data.prediction.PredictionTeam;
import com.betting.storage.fail.BusinessFail;
import com.betting.storage.monitoring.Monitoring;
import com.betting.storage.service.prediction.PredictionStorageService;
import com.betting.storage.util.HashingService;
import lombok.ToString;

import java.time.LocalDateTime;
import java.time.ZoneOffset;
import java.util.ArrayList;
import java.util.List;

@ToString
public class PredictionCsvFile {

    private final PredictionStorageService storage;
    private final HashingService hashingService;
    private final List<PredictionCsvFileLine> lines = new ArrayList<>();

    public PredictionCsvFile(PredictionStorageService storage, HashingService hashingService, String body) {
        this.storage = storage;
        this.hashingService = hashingService;
        String[] lines = body.split("([\r\n]+)");
        int i = 1;
        for (String line : lines) {
            addLine(i, line.split(",", -1));
            i++;
        }
    }

    public PredictionCsvFile addLine(int line, String... columns) {
        lines.add(new PredictionCsvFileLine(line, columns));
        return this;
    }

    public List<PredictionMatch> toDbData() {
        List<PredictionTeam> teams = storage.getTeams();
        List<PredictionMatch> result = new ArrayList<>();
        PredictionMatch match = null;
        boolean readingTeam1 = true;
        for (int i = 1; i < lines.size(); i++) {
            PredictionCsvFileLine line = lines.get(i);
            PredictionCsvFileLine previousLine = lines.get(i - 1);
            if (line.isMatch() && previousLine.isEmpty()) {
                PredictionTeam team1 = teams.stream().filter(t -> t.getNames().stream().anyMatch(tn -> tn.equalsIgnoreCase(line.column1))).findFirst()
                        .orElseThrow(() -> BusinessFail.TEAM_NOT_FOUND.exception(Monitoring.BusinessObject.PREDICTION, "Import data failed validation", line.column1, getClass()));
                PredictionTeam team2 = teams.stream().filter(t -> t.getNames().stream().anyMatch(tn -> tn.equalsIgnoreCase(line.column2))).findFirst()
                        .orElseThrow(() -> BusinessFail.TEAM_NOT_FOUND.exception(Monitoring.BusinessObject.PREDICTION, "Import data failed validation", line.column2, getClass()));
                String stringStartDate = line.column5;
                String externalMatchId = hashingService.hash(line.column6);
                match = new PredictionMatch(externalMatchId, team1, team2, LocalDateTime.parse(stringStartDate).atZone(ZoneOffset.UTC).toLocalDateTime());
                result.add(match);
                readingTeam1 = true;
                continue;
            }
            if (!line.isEmpty() && match != null) {
                String[] player = line.column1.split(" ");
                PredictionPlayerName playerObj = new PredictionPlayerName(player[0], player[1]);
                Double probability = PredictionCsvFileLine.numberOrThrow(line.column2);
                Double playTime = PredictionCsvFileLine.numberOrThrow(line.column3);
                Double points = PredictionCsvFileLine.numberOrThrow(line.column4);
                String externalId = hashingService.hash(line.column5);
                if (readingTeam1) {
                    match.addTeam1Prediction(new Prediction(playerObj, probability, playTime, points, externalId));
                } else {
                    match.addTeam2Prediction(new Prediction(playerObj, probability, playTime, points, externalId));
                }
            }
            if (line.isEmpty() && !previousLine.isMatch()&& !previousLine.isEmpty()) {
                readingTeam1 = false;
            }
        }
        return result;
    }
}
