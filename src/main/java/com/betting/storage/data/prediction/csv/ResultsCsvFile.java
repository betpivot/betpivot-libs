package com.betting.storage.data.prediction.csv;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class ResultsCsvFile {

    public final List<ResultsCsvFileLine> lines = new ArrayList<>();

    public ResultsCsvFile(String body) {
        String[] lines = body.split("([\r\n]+)");
        Arrays.stream(lines).forEach(l -> addLine(l.split(",", -1)));
    }


    public ResultsCsvFile addLine(String... line) {
        lines.add(new ResultsCsvFileLine(line));
        return this;
    }
}
