package com.betting.storage.data.prediction;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.bson.types.ObjectId;
import org.mongodb.morphia.annotations.Embedded;
import org.mongodb.morphia.annotations.Id;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class Prediction {

    @Id
    private ObjectId id;

    @Embedded
    private PredictionPlayerName player;
    private Double probability;
    private Double playTime;
    private Double points;
    private String externalId;

    public Prediction(PredictionPlayerName player, Double probability, Double playTime, Double points, String externalId) {
        this.player = player;
        this.probability = probability;
        this.playTime = playTime;
        this.points = points;
        this.externalId = externalId;
    }
}
