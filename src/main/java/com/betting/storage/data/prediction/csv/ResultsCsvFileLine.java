package com.betting.storage.data.prediction.csv;

public class ResultsCsvFileLine {
    public final String column1;
    public final String column2;
    public final String column3;
    public final String column4;
    public final String column5;
    public final String column6;
    public final String column7;
    public final String column8;

    public ResultsCsvFileLine(String... columns) {
        if (columns.length >= 4) {
            this.column1 = cleanString(columns[0]);
            this.column2 = cleanString(columns[1]);
            this.column3 = cleanString(columns[2]);
            this.column4 = cleanString(columns[3]);
            this.column5 = cleanString(columns[4]);
            this.column6 = cleanString(columns[5]);
            this.column7 = cleanString(columns[6]);
            this.column8 = cleanString(columns[7]);
        } else {
            this.column1 = "";
            this.column2 = "";
            this.column3 = "";
            this.column4 = "";
            this.column5 = "";
            this.column6 = "";
            this.column7 = "";
            this.column8 = "";
        }
    }

    public boolean isFirst() {
        return column1.contains("ID");
    }

    private String cleanString(String s) {
        return s == null ? null : s.replace("\"", "");
    }
}

