package com.betting.storage.data.prediction;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class PredictionPlayerName {
    private String firstName;
    private String lastName;

    public String getFullName() {
        return getFirstName() + " " + getLastName();
    }
}
