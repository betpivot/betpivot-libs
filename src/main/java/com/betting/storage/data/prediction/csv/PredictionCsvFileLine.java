package com.betting.storage.data.prediction.csv;

import com.betting.storage.fail.BusinessFail;
import com.betting.storage.monitoring.Monitoring;

import java.util.Arrays;

public class PredictionCsvFileLine {
    public final String column1;
    public final String column2;
    public final String column3;
    public final String column4;
    public final String column5;
    public final String column6;

    public PredictionCsvFileLine(int line, String... columns) {
        if (columns.length < 6) {
            throw BusinessFail.PREDICTION_IMPORT_VALIDATION
                    .exception(
                            Monitoring.BusinessObject.PREDICTION,
                            "Found less columns then 5 on line " + line,
                            Arrays.asList(columns).toString(),
                            getClass());
        }
        this.column1 = cleanString(columns[0]);
        this.column2 = cleanString(columns[1]);
        this.column3 = cleanString(columns[2]);
        this.column4 = cleanString(columns[3]);
        this.column5 = cleanString(columns[4]);
        this.column6 = cleanString(columns[5]);
    }

    private String cleanString(String s) {
        return s == null ? null : s.replace("\"", "");
    }

    public static Double numberOrThrow(String column) {
        try {
            return Double.valueOf(column);
        } catch (NumberFormatException e) {
            throw BusinessFail.PREDICTION_IMPORT_VALIDATION
                    .exception(
                            Monitoring.BusinessObject.PREDICTION,
                            "Column value cannot be assigned to double",
                            column,
                            PredictionCsvFileLine.class, e);
        }
    }

    public boolean isMatch() {
        return !isEmpty() && column1.matches("[A-Z]{2,3}") && column2.matches("[A-Z]{2,3}");
    }

    public boolean isEmpty() {
        return isEmpty(column1) && isEmpty(column2) && isEmpty(column3) && isEmpty(column4) && isEmpty(column5) && isEmpty(column6);
    }

    public boolean isEmpty(String s) {
        return s == null || s.isEmpty();
    }
}
