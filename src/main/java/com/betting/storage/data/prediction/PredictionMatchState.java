package com.betting.storage.data.prediction;

public enum PredictionMatchState {
    ACTIVE,
    RETIRED,
    EVALUATED;
}
