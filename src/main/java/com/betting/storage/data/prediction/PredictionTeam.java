package com.betting.storage.data.prediction;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.bson.types.ObjectId;
import org.mongodb.morphia.annotations.*;

import java.util.ArrayList;
import java.util.List;

@Entity
@Data
@NoArgsConstructor
@AllArgsConstructor
@Indexes({
        @Index(fields = { @Field("nameId")}),
        @Index(fields = { @Field("names")})
})
public class PredictionTeam {

    @Id
    private ObjectId id;

    private String nameId;

    @Embedded
    private List<String> names = new ArrayList<>();


}
