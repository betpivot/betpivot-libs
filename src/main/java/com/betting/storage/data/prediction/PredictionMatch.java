package com.betting.storage.data.prediction;

import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.bson.types.ObjectId;
import org.mongodb.morphia.annotations.*;

import java.text.Normalizer;
import java.time.LocalDateTime;
import java.time.ZoneOffset;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.function.Predicate;
import java.util.stream.Collectors;

@Entity
@Data
@NoArgsConstructor
@Indexes({
        @Index(fields = { @Field("state")}),
        @Index(fields = { @Field("state"), @Field("externalMatchId")}),
        @Index(fields = { @Field("externalMatchId")}),
        @Index(fields = { @Field("team1Predictions")}),
        @Index(fields = { @Field("team2Predictions")})
})
@Slf4j
public class PredictionMatch {

    @Id
    private ObjectId id;

    private String externalMatchId;

    @Reference
    private PredictionTeam team1;

    @Reference
    private PredictionTeam team2;

    private LocalDateTime importTime;

    private LocalDateTime startTime;

    @Embedded
    private List<Prediction> team1Predictions = new ArrayList<>();

    @Embedded
    private List<Prediction> team2Predictions = new ArrayList<>();

    private PredictionMatchState state;

    public PredictionMatch(String externalMatchId, PredictionTeam team1, PredictionTeam team2, LocalDateTime startTime) {
        this.externalMatchId = externalMatchId;
        this.team1 = team1;
        this.team2 = team2;
        this.startTime = startTime;
        this.importTime = LocalDateTime.now(ZoneOffset.UTC);
        this.state = PredictionMatchState.ACTIVE;
    }

    public void addTeam1Prediction(Prediction prediction) {
        team1Predictions.add(prediction);
    }

    public void addTeam2Prediction(Prediction prediction) {
        team2Predictions.add(prediction);
    }

    public Optional<Prediction> getPrediction(String firsName, String lastName, String teamNameId, boolean silently, String logReportName) {

        String matchingLastName = Normalizer
                .normalize(lastName.toLowerCase(), Normalizer.Form.NFD)
                .replaceAll("[^\\p{ASCII}]", "");

        String matchingFirstName = Normalizer
                .normalize(firsName.toLowerCase().replace(".", ""), Normalizer.Form.NFD)
                .replaceAll("[^\\p{ASCII}]", "");

        boolean matchFullName = matchingFirstName.length() > 1;

        Predicate<Prediction> fullNamePredicate = p -> p.getPlayer().getFirstName().replace(".", "").equalsIgnoreCase(matchingFirstName) && p.getPlayer().getLastName().equalsIgnoreCase(matchingLastName);
        Optional<Prediction> fullName = getForTeam(fullNamePredicate, teamNameId);
        if (fullName.isPresent()) {
            return fullName;
        }
        if (matchFullName) {
            Predicate<Prediction> containsPredicate = p -> p.getPlayer().getFirstName().replace(".", "").toLowerCase().contains(matchingFirstName) && p.getPlayer().getLastName().equalsIgnoreCase(matchingLastName);
            Optional<Prediction> containsName = getForTeam(containsPredicate, teamNameId);
            if (containsName.isPresent()) {
                return containsName;
            }
        } else {
            Predicate<Prediction> matchStartOfFirstName = p -> p.getPlayer().getFirstName().replace(".", "").toLowerCase().startsWith(matchingFirstName) && p.getPlayer().getLastName().equalsIgnoreCase(matchingLastName);
            Optional<Prediction> startOfFirstName = getForTeam(matchStartOfFirstName, teamNameId);
            if (startOfFirstName.isPresent()) {
                return startOfFirstName;
            }
        }

        String predictions = getAllPredictions().stream()
                .map(p -> p.getPlayer().getFullName())
                .collect(Collectors.joining(", "));
        if (!silently) {
            log.error("No prediction found for [{} {}, {}] in [{} vs {}: {}] for provider {}", firsName, lastName, teamNameId, team1.getNameId(), team2.getNameId(), predictions, logReportName);
        }

        return Optional.empty();
    }

    private Optional<Prediction> getForTeam(Predicate<Prediction> predicate, String teamNameId) {
        if (team1.getNameId().equalsIgnoreCase(teamNameId)) {
            return team1Predictions.stream().filter(predicate).findAny();
        }
        if (team2.getNameId().equalsIgnoreCase(teamNameId)) {
            return team2Predictions.stream().filter(predicate).findAny();
        }

        return getAllPredictions().stream().filter(predicate).findAny();
    }


    public List<Prediction> getAllPredictions() {
        ArrayList<Prediction> suggestions = new ArrayList<>(getTeam1Predictions());
        suggestions.addAll(getTeam2Predictions());
        return suggestions;
    }

    public Optional<Prediction> getSuggestion(String firstname, String surname) {
        return getAllPredictions().stream()
                .filter(s -> s.getPlayer().getLastName().equals(surname))
                .findAny();
    }
}
