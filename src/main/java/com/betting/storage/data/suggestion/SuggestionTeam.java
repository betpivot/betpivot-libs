package com.betting.storage.data.suggestion;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.ArrayList;
import java.util.List;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class SuggestionTeam {

    private String nameId;

    private List<SuggestionPlayer> players = new ArrayList<>();

    public SuggestionTeam(String nameId) {
        this.nameId = nameId;
    }
}
