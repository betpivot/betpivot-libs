package com.betting.storage.data.suggestion;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class Suggestion {
    private String providerId;
    private SuggestionType type;
    private Double dontBetUnder;
    private Double rate;
    private Double line;
    private SuggestionState state;
    private String betLink;
    private String betDeepLink;

    private Double probability;

    public static Suggestion ofWaitingForProvider(String providerId) {
        return new Suggestion(providerId, null, null, null, null, SuggestionState.WAITING_FOR_PROVIDER, null, null, null);
    }

    public static Suggestion ofProfitable(String providerId, SuggestionType type, Double dontBetUnder, Double rate, Double line, String betLink, String betDeepLink, Double probability) {
        return new Suggestion(providerId, type, dontBetUnder, rate, line, SuggestionState.PROFITABLE, betLink, betDeepLink, probability);
    }


    public static Suggestion ofNotProfitable(String providerId, SuggestionType type, Double dontBetUnder, Double rate, Double line, String betLink, String betDeepLink, Double probability) {
        return new Suggestion(providerId, type, dontBetUnder, rate, line, SuggestionState.NOT_PROFITABLE, betLink, betDeepLink, probability);
    }

    public static Suggestion ofMissingInProvider(String providerId) {
        return new Suggestion(providerId, null, null, null, null, SuggestionState.MISSING_IN_PROVIDER, null, null, null);
    }

    public static Suggestion ofParsingFailed(String providerId) {
        return new Suggestion(providerId, null, null, null, null, SuggestionState.PARSING_FAILED, null, null, null);
    }

    public static Suggestion ofWillNotPlay(String providerId) {
        return new Suggestion(providerId, null, null, null, null, SuggestionState.WILL_NOT_PLAY, null, null, null);
    }
}
