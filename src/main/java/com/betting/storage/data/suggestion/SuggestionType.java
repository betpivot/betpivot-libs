package com.betting.storage.data.suggestion;

public enum SuggestionType {
    LESS,
    MORE;
}
