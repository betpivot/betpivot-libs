package com.betting.storage.data.suggestion;

public enum SuggestionState {
    WILL_NOT_PLAY(0),
    PROFITABLE(10),
    WAITING_FOR_PROVIDER(20),
    PARSING_FAILED(30),
    NOT_PROFITABLE(40),
    MISSING_IN_PROVIDER(50);

    public final int order;

    SuggestionState(int order) {
        this.order = order;
    }

}
