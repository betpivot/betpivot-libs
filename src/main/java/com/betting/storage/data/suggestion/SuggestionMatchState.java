package com.betting.storage.data.suggestion;

public enum SuggestionMatchState {
    ACTIVE,
    RETIRED,
    EVALUATED;
}
