package com.betting.storage.data.suggestion;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.ArrayList;
import java.util.List;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class SuggestionPlayer {
    private String playerName;
    private String externalId;
    private List<Suggestion> suggestions = new ArrayList<>();
}
