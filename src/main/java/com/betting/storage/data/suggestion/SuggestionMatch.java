package com.betting.storage.data.suggestion;

import lombok.Data;
import lombok.NoArgsConstructor;
import org.bson.types.ObjectId;
import org.mongodb.morphia.annotations.*;

import java.time.LocalDateTime;
import java.time.ZoneOffset;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;
import java.util.stream.Stream;

@Entity
@Data
@NoArgsConstructor
@Indexes({
        @Index(fields = {@Field("state")}),
        @Index(fields = {@Field("externalMatchId")}),
        @Index(fields = {@Field("state"), @Field("externalMatchId")}),

})
public class SuggestionMatch {

    @Id
    private ObjectId id;

    private String externalMatchId;

    private LocalDateTime importTime;

    private LocalDateTime startTime;

    private SuggestionMatchState state;

    @Embedded
    private SuggestionTeam team1;

    @Embedded
    private SuggestionTeam team2;

    public SuggestionMatch(String externalMatchId, LocalDateTime startTime, SuggestionTeam team1, SuggestionTeam team2) {
        this.externalMatchId = externalMatchId;
        this.startTime = startTime;
        this.team1 = team1;
        this.team2 = team2;
        this.importTime = LocalDateTime.now(ZoneOffset.UTC);
        this.state = SuggestionMatchState.ACTIVE;
    }

    public Optional<SuggestionTeam> belongsToTeam(SuggestionPlayer player) {
        return Optional
                .ofNullable(team1.getPlayers().contains(player) ? team1 : team2.getPlayers().contains(player) ? team2 : null);
    }

    public Optional<SuggestionPlayer> getPlayerByExternalId(String externalId) {
        return Stream
                .concat(team1.getPlayers().stream(), team2.getPlayers().stream())
                .filter(p -> p.getExternalId().equals(externalId)).findAny();
    }
}
