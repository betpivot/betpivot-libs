package com.betting.storage.data.performance;

import com.betting.storage.data.suggestion.SuggestionType;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.bson.types.ObjectId;
import org.mongodb.morphia.annotations.*;

import java.time.LocalDateTime;
import java.time.ZonedDateTime;

@Data
@Entity
@AllArgsConstructor
@NoArgsConstructor
@Indexes({
        @Index(fields = { @Field("active")}),
        @Index(fields = { @Field("persistent")}),
        @Index(fields = { @Field("betImportTime")}),
        @Index(fields = { @Field("externalMatchId")}),
        @Index(fields = { @Field("active"), @Field("persistent"), @Field("betImportTime")})
})
public class PerformanceResult {

    @Id
    private ObjectId id;

    private String playerName;
    private String teamNameId;
    private String providerId;
    private SuggestionType type;
    private Double dontBetUnder;
    private Double rate;
    private Double line;

    private Integer result;
    private LocalDateTime betImportTime;
    private LocalDateTime predictionEvaluationTime;

    private PerformanceResultValue resultValue;

    private Double probability;
    private String externalId;
    private String externalMatchId;

    private boolean active;
    private boolean persistent;

    public PerformanceResult(String playerName, String teamNameId, String providerId, SuggestionType type, Double dontBetUnder, Double rate, Double line, Integer result, LocalDateTime betImportTime, LocalDateTime predictionEvaluationTime, PerformanceResultValue resultValue, Double probability, String externalId, String externalMatchId) {
        this.playerName = playerName;
        this.teamNameId = teamNameId;
        this.providerId = providerId;
        this.type = type;
        this.dontBetUnder = dontBetUnder;
        this.rate = rate;
        this.line = line;
        this.result = result;
        this.betImportTime = betImportTime;
        this.predictionEvaluationTime = predictionEvaluationTime;
        this.resultValue = resultValue;
        this.probability = probability;
        this.externalId = externalId;
        this.externalMatchId = externalMatchId;
        this.active = true;
        this.persistent = false;
    }
}
