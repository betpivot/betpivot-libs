package com.betting.storage.data.performance;

public enum PerformanceResultValue {
    WINNING(1),
    NOT_WINNING(2),
    NOT_PLAYED(3),
    NOT_EVALUATED(4),
    WAITING_FOR_EVALUATION(5);

    public final int order;

    PerformanceResultValue(int order) {
        this.order = order;
    }
}
