package com.betting.storage.data.config;

import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
public class UserConfiguration {

    private String privacyConsentUrl;

    private String unlockAdUnitId;
}
