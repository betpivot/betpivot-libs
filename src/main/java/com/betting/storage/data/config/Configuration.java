package com.betting.storage.data.config;

import lombok.Data;
import lombok.NoArgsConstructor;
import org.bson.types.ObjectId;
import org.mongodb.morphia.annotations.Embedded;
import org.mongodb.morphia.annotations.Entity;
import org.mongodb.morphia.annotations.Id;

import java.util.List;

@Entity
@Data
@NoArgsConstructor
public class Configuration {

    @Id
    private ObjectId id;

    @Embedded
    private UserConfiguration user;

    @Embedded
    private LocalizationConfiguration localization;
}
