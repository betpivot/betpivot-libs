package com.betting.storage.data.config;

public enum SeasonState {
    RUNNING,
    NOT_RUNNING;
}
