package com.betting.storage.data.config;

import lombok.Data;

@Data
public class LocalizationConfiguration {

    private String teamJoinCharacter;
    private TeamOrder teamOrder;

    public enum TeamOrder {
        TEAM1_FIRST,
        TEAM2_FIRST;
    }
}
