package com.betting.storage.data.config;

import lombok.Data;
import lombok.NoArgsConstructor;
import org.bson.types.ObjectId;
import org.mongodb.morphia.annotations.*;

import java.time.LocalDateTime;
import java.time.LocalTime;

@Entity
@Data
@NoArgsConstructor
@Indexes({
        @Index(fields = { @Field("seasonId")}, unique = true),
        @Index(fields = { @Field("state")}),
        @Index(fields = { @Field("start")}),
        @Index(fields = { @Field("end")}),
        @Index(fields = { @Field("state"), @Field("start"), @Field("end")})
})
public class SeasonConfiguration {

    @Id
    private ObjectId id;

    private String seasonId;

    private SeasonState state;
    private LocalDateTime start;
    private LocalDateTime end;
    private LocalTime predictionImportTime;
}
