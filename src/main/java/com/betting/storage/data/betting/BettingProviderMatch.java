package com.betting.storage.data.betting;


import com.google.common.base.Strings;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.bson.types.ObjectId;
import org.mongodb.morphia.annotations.*;

import java.time.LocalDateTime;
import java.time.ZoneOffset;
import java.util.*;
import java.util.stream.Collectors;

@Entity
@Data
@NoArgsConstructor
@AllArgsConstructor
@Indexes({
        @Index(fields = {@Field("active")}),
        @Index(fields = {@Field("team1NameId")}),
        @Index(fields = {@Field("team2NameId")}),
        @Index(fields = {@Field("externalMatchId")}),
        @Index(fields = {@Field("active"), @Field("provider")}),
        @Index(fields = {@Field("active"), @Field("externalMatchId")}),
        @Index(fields = {@Field("active"), @Field("provider"), @Field("team1NameId"), @Field("team2NameId")})
})
@Slf4j
public class BettingProviderMatch {

    @Id
    private ObjectId id;

    private String externalMatchId;

    private String team1NameId;
    private String team2NameId;

    @Reference
    private BettingProvider provider;

    @Embedded
    private List<BettingProviderBet> bets = new ArrayList<>();

    private boolean active;

    private LocalDateTime importTime;

    public BettingProviderMatch(String team1NameId, String team2NameId, BettingProvider provider, String externalMatchId) {
        this.team1NameId = team1NameId;
        this.team2NameId = team2NameId;
        this.provider = provider;
        this.externalMatchId = externalMatchId;
        this.active = true;
        this.importTime = LocalDateTime.now(ZoneOffset.UTC);
    }

    public List<BettingProviderBet> getByExternalId(String externalId) {
        return bets.stream()
                .filter(b -> externalId.equalsIgnoreCase(b.getExternalId()))
                .collect(Collectors.toList());
    }

    public void addBet(BettingProviderBet bet) {
        bets.add(bet);
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        BettingProviderMatch match = (BettingProviderMatch) o;
        return Objects.equals(externalMatchId, match.externalMatchId) &&
                Objects.equals(team1NameId, match.team1NameId) &&
                Objects.equals(team2NameId, match.team2NameId) &&
                Objects.equals(provider, match.provider) &&
                Objects.equals(bets, match.bets);
    }

    @Override
    public int hashCode() {
        return Objects.hash(externalMatchId, team1NameId, team2NameId, provider, bets);
    }
}
