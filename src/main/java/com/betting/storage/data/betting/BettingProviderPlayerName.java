package com.betting.storage.data.betting;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class BettingProviderPlayerName {
    private String firstName;
    private String lastName;
}
