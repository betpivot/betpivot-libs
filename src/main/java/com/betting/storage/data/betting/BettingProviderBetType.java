package com.betting.storage.data.betting;

public enum BettingProviderBetType {
    MORE,
    LESS;

    public boolean more() {
        return MORE.equals(this);
    }

    public boolean less() {
        return LESS.equals(this);
    }
}
