package com.betting.storage.data.betting;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.bson.types.ObjectId;
import org.mongodb.morphia.annotations.*;

@Entity
@Data
@NoArgsConstructor
@AllArgsConstructor
@Indexes({
        @Index(fields = {@Field("name"), @Field("active")}),
        @Index(fields = { @Field("name")}),
        @Index(fields = { @Field("active")}),
        @Index(fields = { @Field("sort")}, unique = true)
})
public class BettingProvider {

    @Id
    private ObjectId id;
    private String name;
    private String logo;
    private String icon;
    private String color;
    private String registrationUrl;
    private Integer sort;

    private boolean active;

    public String getNotifyName() {
        if (name != null) {
            switch (name) {
                case "Tipsport" : return "Tipsportu";
                case "Fortuna" : return "Fortuny";
                case "Chance" : return "Chance";
                case "SazkaBet" : return "SazkaBet";
            }
        }
        return name;
    }
}
