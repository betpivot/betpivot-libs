package com.betting.storage.data.betting;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.mongodb.morphia.annotations.Embedded;

import java.time.LocalDateTime;
import java.util.Objects;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class BettingProviderBet {

    @Embedded
    private BettingProviderPlayerName playerName;

    private Double line;
    private BettingProviderBetType type;

    private Double rate;

    private String betLinkUrl;
    private String betDeepLink;

    private String externalId;
    private LocalDateTime imporTime;


    public BettingProviderBet(BettingProviderPlayerName playerName, Double line, BettingProviderBetType type, Double rate, String betLinkUrl, String betDeepLink, String externalId) {
        this.playerName = playerName;
        this.line = line;
        this.type = type;
        this.rate = rate;
        this.betLinkUrl = betLinkUrl;
        this.betDeepLink = betDeepLink;
        this.externalId = externalId;
        this.imporTime = LocalDateTime.now();
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        BettingProviderBet that = (BettingProviderBet) o;
        return Objects.equals(playerName, that.playerName) &&
                Objects.equals(line, that.line) &&
                type == that.type &&
                Objects.equals(rate, that.rate);
    }

    @Override
    public int hashCode() {
        return Objects.hash(playerName, line, type, rate);
    }
}
