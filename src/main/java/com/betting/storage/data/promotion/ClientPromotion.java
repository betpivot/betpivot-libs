package com.betting.storage.data.promotion;

import lombok.Data;
import lombok.NoArgsConstructor;
import org.bson.types.ObjectId;
import org.mongodb.morphia.annotations.*;

import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;

@Entity
@Data
@NoArgsConstructor
@Indexes({
        @Index(fields = {@Field("state")}),
        @Index(fields = {@Field("start")}),
        @Index(fields = {@Field("end")}),
        @Index(fields = {@Field("promoCode")}, unique = true),
        @Index(fields = {@Field("clientId")}, unique = true),
        @Index(fields = {@Field("promoCode"), @Field("clientId")}, unique = true),
        @Index(fields = {@Field("state"), @Field("promoCode"), @Field("start"),  @Field("end")}),

})
public class ClientPromotion {
    @Id
    private ObjectId id;

    private LocalDateTime start;
    private LocalDateTime end;
    private PromotionState state;

    private String promoCode;
    private String clientId;

    @Reference
    private List<ClientPromotionUsage> usedBy = new ArrayList<>();

    public ClientPromotion(String clientId) {
        this.promoCode = ObjectId.get().toHexString();
        this.clientId = clientId;
        this.state = PromotionState.ENABLED;
    }

    public boolean usedByClientId(String clientId) {
        return usedBy.stream().anyMatch(u -> u.getClientId().equalsIgnoreCase(clientId));
    }
}
