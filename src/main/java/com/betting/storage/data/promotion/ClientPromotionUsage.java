package com.betting.storage.data.promotion;

import lombok.Data;
import lombok.NoArgsConstructor;
import org.bson.types.ObjectId;
import org.mongodb.morphia.annotations.*;

import java.time.LocalDateTime;
import java.time.ZoneOffset;

@Entity
@Data
@NoArgsConstructor
@Indexes({
        @Index(fields = {@Field("clientId")}, unique = true),

})
public class ClientPromotionUsage {

    @Id
    private ObjectId id;

    private String clientId;
    private LocalDateTime useTime;

    public ClientPromotionUsage(String clientId) {
        this.clientId = clientId;
        this.useTime = LocalDateTime.now(ZoneOffset.UTC);
    }
}
