package com.betting.storage.data.promotion;

import lombok.Data;
import lombok.NoArgsConstructor;
import org.bson.types.ObjectId;
import org.mongodb.morphia.annotations.*;

import java.time.LocalDateTime;

@Entity
@Data
@NoArgsConstructor
@Indexes({
        @Index(fields = {@Field("state")}),
        @Index(fields = {@Field("start")}),
        @Index(fields = {@Field("end")}),
        @Index(fields = {@Field("promoCode")}, unique = true),
        @Index(fields = {@Field("state"), @Field("promoCode"), @Field("start"),  @Field("end")}),

})
public class Promotion {

    @Id
    private ObjectId id;

    private LocalDateTime start;
    private LocalDateTime end;
    private PromotionState state;

    private String promoCode;
}
