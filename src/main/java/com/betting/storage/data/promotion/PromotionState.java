package com.betting.storage.data.promotion;

public enum PromotionState {
    ENABLED,
    DISABLED;
}
