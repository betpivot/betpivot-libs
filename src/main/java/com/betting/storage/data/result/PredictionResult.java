package com.betting.storage.data.result;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.bson.types.ObjectId;
import org.mongodb.morphia.annotations.*;

import java.time.LocalDateTime;
import java.time.ZoneOffset;
import java.time.ZonedDateTime;

@AllArgsConstructor
@NoArgsConstructor
@Data
public class PredictionResult {

    private Integer points;
    private Boolean played;
    private String externalId;
    private String externalMatchId;
    private LocalDateTime importTime;

    public PredictionResult(Integer points, Boolean played, String externalId, String externalMatchId) {
        this.points = points;
        this.played = played;
        this.externalId = externalId;
        this.externalMatchId = externalMatchId;
        this.importTime = LocalDateTime.now(ZoneOffset.UTC);
    }
}
