package com.betting.storage.util;

public interface HashingService {

    String hash(String input);
}
