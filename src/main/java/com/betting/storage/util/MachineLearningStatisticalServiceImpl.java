package com.betting.storage.util;

import com.betting.storage.data.betting.BettingProviderBet;
import com.betting.storage.data.prediction.Prediction;
import lombok.Setter;
import org.apache.commons.io.IOUtils;
import org.lightgbm.predict4j.SparseVector;
import org.lightgbm.predict4j.v2.Boosting;
import org.lightgbm.predict4j.v2.GBDT;
import org.lightgbm.predict4j.v2.OverallConfig;
import org.lightgbm.predict4j.v2.Predictor;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Primary;
import org.springframework.core.io.Resource;
import org.springframework.stereotype.Service;

import javax.annotation.PostConstruct;
import java.io.IOException;
import java.math.BigDecimal;
import java.time.LocalDateTime;
import java.time.ZoneOffset;
import java.time.temporal.ChronoUnit;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Service
@Primary
public class MachineLearningStatisticalServiceImpl implements StatisticalService {

    @Value("classpath:public/model/betting_model4.txt")
    @Setter
    private Resource model;

    private Predictor predictor;


    @Override
    public double getProbability(Prediction prediction, BettingProviderBet bet) {

        LocalDateTime now = LocalDateTime.now(ZoneOffset.UTC);
        double var1 = now.getMonthValue() > 7  ? now.getYear() : now.minus(1, ChronoUnit.YEARS).getYear();
        double var2 = bet.getLine();
        double var3 = prediction.getPoints();
        double var4 = prediction.getPlayTime();
        double var5 = prediction.getProbability();
        double var6 = prediction.getPoints() - bet.getLine();
        double var7 = Math.signum(var6);
        double var8 = Math.abs(var6);
        double var9 = divide(prediction.getPoints(), Math.max(0.1, prediction.getPlayTime()));
        double var10 = divide(prediction.getPoints(),  bet.getLine());
        double var11 = ((int) bet.getLine().doubleValue()) % 2;
        double var12 = prediction.getPoints() * prediction.getProbability();


        // your data to predict
        int[] indices = {0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11};
        double[] values = {var1, var2, var3, var4, var5, var6, var7, var8, var9, var10, var11, var12};

        SparseVector v = new SparseVector(values, indices);
        List<Double> predicts = predictor.predict(v);
        return predicts.isEmpty() ? 0.0 : predicts.get(0);
    }

    @PostConstruct
    private void init() {
        Boosting boosting = new GBDT();
        try {
            String stringModel = String.join("\n", IOUtils.readLines(model.getInputStream()));
            boosting.loadModelFromString(stringModel);
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
        // predict config, just like predict.conf in LightGBM
        Map<String, String> map = new HashMap<>();
        OverallConfig config = new OverallConfig();
        config.set(map);
        // create predictor
        predictor =  new Predictor(boosting, config.io_config.num_iteration_predict, config.io_config.is_predict_raw_score,
                config.io_config.is_predict_leaf_index, config.io_config.pred_early_stop,
                config.io_config.pred_early_stop_freq, config.io_config.pred_early_stop_margin);
    }

    private double divide(Double divide, Double by) {
        return new BigDecimal(divide).divide( new BigDecimal(by), 20, BigDecimal.ROUND_HALF_UP).doubleValue();
    }
}
