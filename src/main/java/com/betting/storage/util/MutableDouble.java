package com.betting.storage.util;

public class MutableDouble {
    private Double impl;

    public MutableDouble(double impl) {
        this.impl = impl;
    }

    public MutableDouble() {
       this(0.0);
    }

    public MutableDouble add(double add) {
        impl = impl + add;
        return this;
    }

    public double val() {
        return impl;
    }
}
