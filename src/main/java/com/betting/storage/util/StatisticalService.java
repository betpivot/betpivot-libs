package com.betting.storage.util;

import com.betting.storage.data.betting.BettingProviderBet;
import com.betting.storage.data.prediction.Prediction;

public interface StatisticalService {

    double getProbability(Prediction prediction, BettingProviderBet bet);
}
