package com.betting.storage.util;

import com.betting.storage.data.betting.BettingProviderBet;
import com.betting.storage.data.prediction.Prediction;
import org.springframework.stereotype.Service;
import umontreal.ssj.probdist.NegativeBinomialDist;

import java.math.BigDecimal;

@Service
public class StatisticalServiceImpl implements StatisticalService {

    private static final BigDecimal na = new BigDecimal(0.4068858584641312);
    private static final BigDecimal nb = new BigDecimal(0.14848145861347406);
    private static final BigDecimal nc = new BigDecimal(0.032813943001605346);
    private static final BigDecimal nd = new BigDecimal(0.00043201559793602454);

    private static final BigDecimal pa = new BigDecimal(0.2827132754710447);
    private static final BigDecimal pb = new BigDecimal(0.007708462080012921);
    private static final BigDecimal pc = new BigDecimal(0.0017970703388353246);
    private static final BigDecimal pd = new BigDecimal(0.00007885518918335403);
    private static final BigDecimal pe = new BigDecimal(1.0924362730886669);

    public BigDecimal getNParam(Double value) {
        BigDecimal val = new BigDecimal(value);
        return na
                .add(nb.multiply(val))
                .add(nc.multiply(val.pow(2)))
                .subtract(nd.multiply(val.pow(3)));
    }

    public BigDecimal getPParam(Double value) {
        BigDecimal val = new BigDecimal(value);
        return pa
                .subtract(pb.multiply(val))
                .add(pc.multiply(val.pow(2)))
                .subtract(pd.multiply(val.pow(3)))
                .add(pe.scaleByPowerOfTen(-6).multiply(val.pow(4)));
    }

    @Override
    public double getProbability(Prediction prediction, BettingProviderBet bet) {
        return getProbability(prediction.getPoints(), bet.getLine());
    }

    public double getProbability(double points, double line) {
        return getProbability(getNParam(points).doubleValue(), getPParam(points).doubleValue(), line);
    }

    public double getProbability(double n, double p, double line) {
        return NegativeBinomialDist.cdf(n, p, (int) line);
    }
}
