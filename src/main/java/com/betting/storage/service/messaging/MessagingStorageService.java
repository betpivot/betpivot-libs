package com.betting.storage.service.messaging;

import com.betting.storage.data.messaging.BettingMessage;
import com.betting.storage.data.messaging.BettingMessageType;

import java.util.Optional;

public interface MessagingStorageService {
    Optional<BettingMessage>  pullMessage(BettingMessageType topic);
    void postMessage(BettingMessage message);
}
