package com.betting.storage.service.messaging;

import com.betting.storage.data.messaging.BettingMessage;
import com.betting.storage.data.messaging.BettingMessageType;
import lombok.Data;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.*;
import java.util.function.Consumer;

@Service
@Slf4j
public class MessagingSubscriptionService {

    private final MessagingStorageService messagingStorage;

    private final Map<BettingMessageType, Subscription> subscription = new HashMap<>();

    @Autowired
    public MessagingSubscriptionService(MessagingStorageService messagingStorage) {
        this.messagingStorage = messagingStorage;
    }

    public void subscribe(BettingMessageType topic, Consumer<BettingMessage> handler) {
        Subscription sub = subscription.get(topic);
        if (sub == null) {
            Timer timer = new Timer();
            timer.schedule(new TimerTask() {
                @Override
                public void run() {
                    messagingStorage
                            .pullMessage(topic)
                            .ifPresent(m -> {
                                Subscription sub = subscription.get(m.getType());
                                if (sub != null) {
                                    sub.handle(m);
                                } else {
                                    log.error("No handler found for trigger timer for message {}", m);
                                }
                            });
                }
            }, 5000, 5000);
            sub = new Subscription(timer);
            subscription.put(topic, sub);
        }
        subscription.get(topic).addHandler(handler);
    }

    public void unsubscribeAll(BettingMessageType topic) {
        if (!subscription.containsKey(topic)) {
            throw new IllegalArgumentException("Topic is not subcriped");
        }
        subscription.get(topic).destroy();
    }

    @Data
    private static class Subscription {
        private final Timer timer;
        private final List<Consumer<BettingMessage>> handlers = new ArrayList<>();

        private void addHandler(Consumer<BettingMessage> handler) {
            handlers.add(handler);
        }

        public void handle(BettingMessage m) {
            handlers.forEach(h -> h.accept(m));
        }

        public void destroy() {
            timer.cancel();
            handlers.clear();
        }
    }
}
