package com.betting.storage.service.messaging;

import com.betting.storage.data.messaging.BettingMessage;
import com.betting.storage.data.messaging.BettingMessageType;
import org.mongodb.morphia.Datastore;
import org.mongodb.morphia.query.Query;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;

import java.util.Optional;

@Service

public class MongoDbMessagingStorageServiceImpl implements MessagingStorageService {
    private static final Logger LOGGER = LoggerFactory.getLogger(MongoDbMessagingStorageServiceImpl.class);

    @Qualifier("messagingDatastore")
    @Autowired
    private Datastore datastore;

    @Override
    public Optional<BettingMessage> pullMessage(BettingMessageType topic) {
        Query<BettingMessage> q = datastore
                .createQuery(BettingMessage.class)
                .filter("type", topic);
        BettingMessage message = datastore.findAndDelete(q);
        datastore.delete(q);
        return Optional.ofNullable(message);
    }

    @Override
    public void postMessage(BettingMessage message) {
        datastore.save(message);
    }
}
