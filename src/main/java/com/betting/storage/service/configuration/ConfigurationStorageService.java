package com.betting.storage.service.configuration;

import com.betting.storage.data.config.Configuration;
import com.betting.storage.data.config.SeasonConfiguration;

import java.time.LocalDateTime;
import java.util.Optional;

public interface ConfigurationStorageService {
    Optional<Configuration> getConfig();

    Optional<SeasonConfiguration> getSeason(LocalDateTime inDate);
}
