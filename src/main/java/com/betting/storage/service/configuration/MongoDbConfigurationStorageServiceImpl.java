package com.betting.storage.service.configuration;

import com.betting.storage.data.config.Configuration;
import com.betting.storage.data.config.SeasonConfiguration;
import com.betting.storage.data.config.SeasonState;
import com.betting.storage.data.config.UserConfiguration;
import com.betting.storage.monitoring.Monitoring;
import org.mongodb.morphia.Datastore;
import org.mongodb.morphia.query.Query;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;

import java.time.LocalDateTime;
import java.time.LocalTime;
import java.util.List;
import java.util.Optional;

@Service

public class MongoDbConfigurationStorageServiceImpl implements ConfigurationStorageService {
    private static final Logger LOGGER = LoggerFactory.getLogger(MongoDbConfigurationStorageServiceImpl.class);

    @Qualifier("configurationDatastore")
    @Autowired
    private Datastore datastore;

    @Override
    public Optional<Configuration> getConfig() {
        Optional<Configuration> config = Optional.ofNullable(datastore.find(Configuration.class).get());
        LOGGER.info("Loading config: {}", config.map(Configuration::toString).orElse("No config found"));
        return config;
    }

    @Override
    public Optional<SeasonConfiguration> getSeason(LocalDateTime inDate) {
        Query<SeasonConfiguration> query = datastore.find(SeasonConfiguration.class);
        query.and(
                query.criteria("state").equal(SeasonState.RUNNING),
                query.criteria("start").lessThanOrEq(inDate),
                query.criteria("end").greaterThanOrEq(inDate));


        List<SeasonConfiguration> seasons = query.asList();
        LOGGER.info("Loading season config with size {} config: {}", seasons.size(), seasons);
        if (seasons.size() > 1) {
            Monitoring.businessError(Monitoring.BusinessObject.INIT, "Found multiple running seasons","api.init.repository.multipleRunningSeasons", seasons.toString(), getClass());
        }
        return Optional.ofNullable(seasons.size() > 0 ? seasons.get(0) : null);
    }

    private void importConfig() {
        Configuration entity = new Configuration();
        SeasonConfiguration season = new SeasonConfiguration();
        season.setPredictionImportTime(LocalTime.of(9, 30));
        season.setState(SeasonState.RUNNING);
        season.setStart(LocalDateTime.now());
        //entity.setSeason(season);
        UserConfiguration user = new UserConfiguration();
        user.setPrivacyConsentUrl("");
        entity.setUser(user);
        datastore.save(entity);
    }
}
