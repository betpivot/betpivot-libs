package com.betting.storage.service.result;

import com.betting.storage.data.result.PredictionResult;
import com.mongodb.WriteResult;
import org.mongodb.morphia.Datastore;
import org.mongodb.morphia.query.Query;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;

import java.util.Collection;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.stream.Collectors;

@Service
public class MongoDbResultStorageService implements ResultStorageService {

    private static final Logger LOGGER = LoggerFactory.getLogger(MongoDbResultStorageService.class);

    @Qualifier("resultDatastore")
    @Autowired
    private Datastore datastore;

    @Override
    public void saveResult(Map<String, List<PredictionResult>> toDbData) {
        LOGGER.info("Saving: {}", toDbData.size());
        retireAll(toDbData.keySet());
        datastore.save(toDbData.values().stream().flatMap(Collection::stream).collect(Collectors.toList()));
    }

    @Override
    public List<PredictionResult> getByExternalIds(Set<String> externalIds) {
        Query<PredictionResult> q = datastore.createQuery(PredictionResult.class);
        q.criteria("externalId").in(externalIds);
        return q.asList();
    }

    private void retireAll(Set<String> externalMatchIds) {
        Query<PredictionResult> query = datastore.createQuery(PredictionResult.class)
                .field("externalMatchId").in(externalMatchIds);
        WriteResult delete = datastore.delete(query);
        LOGGER.info("Deleted: " + delete);
    }
}
