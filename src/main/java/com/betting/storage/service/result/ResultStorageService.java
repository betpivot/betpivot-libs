package com.betting.storage.service.result;

import com.betting.storage.data.result.PredictionResult;

import java.util.List;
import java.util.Map;
import java.util.Set;

public interface ResultStorageService {

    void saveResult(Map<String, List<PredictionResult>> toDbData);
    List<PredictionResult> getByExternalIds(Set<String> externalIds);
}
