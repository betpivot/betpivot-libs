package com.betting.storage.service.promotion;

import com.betting.storage.data.promotion.ClientPromotion;
import com.betting.storage.data.promotion.ClientPromotionUsage;
import com.betting.storage.data.promotion.Promotion;
import com.betting.storage.data.promotion.PromotionState;
import com.betting.storage.monitoring.Monitoring;
import org.mongodb.morphia.Datastore;
import org.mongodb.morphia.query.Query;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;

import java.time.LocalDateTime;
import java.util.List;
import java.util.Optional;

@Service

public class MongoDbPromotionStorageServiceImpl implements PromotionStorageService {
    private static final Logger LOGGER = LoggerFactory.getLogger(MongoDbPromotionStorageServiceImpl.class);

    @Qualifier("promotionDatastore")
    @Autowired
    private Datastore datastore;


    @Override
    public Optional<Promotion> getActivePromotion(String promoCodeValue, LocalDateTime inDate) {
        Query<Promotion> query = datastore.find(Promotion.class);
        query.and(
                query.criteria("state").equal(PromotionState.ENABLED),
                query.criteria("promoCode").equal(promoCodeValue),
                query.criteria("start").lessThanOrEq(inDate),
                query.criteria("end").greaterThanOrEq(inDate));


        List<Promotion> promotions = query.asList();
        LOGGER.info("Loading promotion with size {} promoCode: {}", promotions.size(), promotions);
        if (promotions.size() > 1) {
            Monitoring.businessError(Monitoring.BusinessObject.INIT, "Found multiple running promotions","api.init.repository.multipleRunningSeasons", promotions.toString(), getClass());
        }
        return Optional.ofNullable(promotions.size() > 0 ? promotions.get(0) : null);
    }

    @Override
    public List<Promotion> getAllActivePromotions(LocalDateTime inDate) {
        Query<Promotion> query = datastore.find(Promotion.class);
        query.and(
                query.criteria("state").equal(PromotionState.ENABLED),
                query.criteria("start").lessThanOrEq(inDate),
                query.criteria("end").greaterThanOrEq(inDate));
        return query.asList();
    }

    @Override
    public Optional<ClientPromotion> getClientPromotionByClientId(String clientId) {
        Query<ClientPromotion> query = datastore.find(ClientPromotion.class);
        query.and(
                query.criteria("state").equal(PromotionState.ENABLED),
                query.criteria("clientId").equal(clientId));
        return Optional.ofNullable(query.get());
    }

    @Override
    public Optional<ClientPromotion> getClientPromotionByCode(String promoCodeValue) {
        Query<ClientPromotion> query = datastore.find(ClientPromotion.class);
        query.and(
                query.criteria("state").equal(PromotionState.ENABLED),
                query.criteria("promoCode").equal(promoCodeValue));
        return Optional.ofNullable(query.get());

    }

    @Override
    public Optional<ClientPromotionUsage> getClientPromotionUsage(String clientId) {
        return Optional.ofNullable(datastore.find(ClientPromotionUsage.class).filter("clientId", clientId).get());
    }

    @Override
    public void save(ClientPromotionUsage usage) {
        datastore.save(usage);
    }

    @Override
    public void save(ClientPromotion promo) {
        datastore.save(promo);
    }
}
