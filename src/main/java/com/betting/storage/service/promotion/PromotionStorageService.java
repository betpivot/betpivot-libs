package com.betting.storage.service.promotion;

import com.betting.storage.data.promotion.ClientPromotion;
import com.betting.storage.data.promotion.ClientPromotionUsage;
import com.betting.storage.data.promotion.Promotion;

import java.time.LocalDateTime;
import java.util.List;
import java.util.Optional;

public interface PromotionStorageService {

    Optional<Promotion> getActivePromotion(String promoCodeValue, LocalDateTime inDate);
    List<Promotion> getAllActivePromotions(LocalDateTime inDate);

    Optional<ClientPromotion> getClientPromotionByClientId(String clientId);

    void save(ClientPromotion promo);

    Optional<ClientPromotion> getClientPromotionByCode(String promoCodeValue);

    Optional<ClientPromotionUsage> getClientPromotionUsage(String clientId);

    void save(ClientPromotionUsage usage);
}
