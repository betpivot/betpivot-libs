package com.betting.storage.service.betting;

import com.betting.storage.data.betting.BettingProvider;
import com.betting.storage.data.betting.BettingProviderMatch;

import java.util.Collection;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.stream.Collectors;

public interface BettingProviderStorageService {
    long saveData(List<BettingProviderMatch> data, BettingProvider provider);

    List<BettingProvider> getActiveProviders();

    Optional<BettingProvider> getActiveProvider(String name);

    Optional<BettingProviderMatch> getMatch(String providerName, String externalMatchId);
    Optional<BettingProviderMatch> getMatch(BettingProvider provider, String externalMatchId);
    List<BettingProviderMatch> getMatches(BettingProvider provider);
    List<BettingProviderMatch> getMatches(String externalMatchId);

    Map<String, BettingProvider> getActiveProvidersMap();
}
