package com.betting.storage.service.betting;

import com.betting.storage.data.betting.BettingProvider;
import com.betting.storage.data.betting.BettingProviderMatch;
import com.betting.storage.util.HashingService;
import com.mongodb.WriteResult;
import org.mongodb.morphia.Datastore;
import org.mongodb.morphia.query.Query;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.context.annotation.Primary;
import org.springframework.stereotype.Service;

import java.util.*;
import java.util.function.Function;
import java.util.stream.Collectors;

@Service
@Primary
public class MongoDbBettingProviderStorageService implements BettingProviderStorageService {

    private static final Logger LOGGER = LoggerFactory.getLogger(MongoDbBettingProviderStorageService.class);

    @Qualifier("bettingDatastore")
    private final Datastore datastore;
    private final HashingService hashingService;

    @Autowired
    public MongoDbBettingProviderStorageService(Datastore datastore, HashingService hashingService) {
        this.datastore = datastore;
        this.hashingService = hashingService;
    }

    @Override
    public long saveData(List<BettingProviderMatch> data, BettingProvider provider) {
        LOGGER.info("Saving: {}", data.size());
        List<String> externalIds = data.stream().map(BettingProviderMatch::getExternalMatchId).collect(Collectors.toList());
        Query<BettingProviderMatch> q = datastore
                .createQuery(BettingProviderMatch.class)
                .filter("active", true)
                .filter("provider", provider)
                .field("externalMatchId").in(externalIds);
        long deactivated = datastore.getCount(q);
        deactivateAllMatches(provider);
        datastore.save(data);

        return deactivated;
    }

    @Override
    public List<BettingProvider> getActiveProviders() {
        return datastore.find(BettingProvider.class)
                .filter("active", true)
                .order("sort")
                .asList();
    }

    @Override
    public Map<String, BettingProvider> getActiveProvidersMap() {
        return getActiveProviders().stream()
                .collect(Collectors.toMap(p -> hashingService.hash(p.getId().toHexString()), Function.identity()));
    }

    @Override
    public Optional<BettingProvider> getActiveProvider(String name) {
        return Optional.ofNullable(datastore.find(BettingProvider.class)
                .filter("active", true)
                .filter("name", name).get());
    }

    @Override
    public Optional<BettingProviderMatch> getMatch(String providerName, String externalMatchId) {
        BettingProvider provider = datastore.find(BettingProvider.class).filter("name", providerName).get();
        return getMatch(provider, externalMatchId);
    }

    @Override
    public Optional<BettingProviderMatch> getMatch(BettingProvider provider, String externalMatchId) {
        if (provider == null) {
            return Optional.empty();
        }
        return Optional.ofNullable(datastore.find(BettingProviderMatch.class)
                .filter("active", true)
                .filter("externalMatchId", externalMatchId)
                .filter("provider", provider).get());
    }

    @Override
    public List<BettingProviderMatch> getMatches(BettingProvider provider) {
        if (provider == null) {
            return Collections.emptyList();
        }
        return datastore.find(BettingProviderMatch.class)
                .filter("active", true)
                .filter("provider", provider).asList();
    }

    @Override
    public List<BettingProviderMatch> getMatches(String externalMatchId) {
        return datastore.find(BettingProviderMatch.class)
                .filter("active", true)
                .filter("externalMatchId", externalMatchId).asList();
    }

    private long deactivateAllMatches(BettingProvider provider, List<String> externalIds) {
        Query<BettingProviderMatch> query = datastore.createQuery(BettingProviderMatch.class)
                .filter("active", true)
                .filter("provider", provider)
                .field("externalMatchId").in(externalIds);
//        UpdateOperations<BettingProviderMatch> ops = datastore
//                .createUpdateOperations(BettingProviderMatch.class)
//                .set("active", false);
        WriteResult update = datastore.delete(query);
        LOGGER.info("Deactivated: " + update);

        return update.getN();
    }

    private long deactivateAllMatches(BettingProvider provider) {
        Query<BettingProviderMatch> q = datastore.createQuery(BettingProviderMatch.class)
                .filter("active", true)
                .filter("provider", provider);
        WriteResult update = datastore.delete(q);
        LOGGER.info("Deactivated: " + update);
        return update.getN();
    }
}
