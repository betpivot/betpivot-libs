package com.betting.storage.service.performance;

import com.betting.storage.data.performance.PerformanceResult;
import com.mongodb.WriteResult;
import org.mongodb.morphia.Datastore;
import org.mongodb.morphia.query.Query;
import org.mongodb.morphia.query.UpdateOperations;
import org.mongodb.morphia.query.UpdateResults;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;

import java.time.LocalDateTime;
import java.util.Comparator;
import java.util.List;
import java.util.Optional;
import java.util.Set;
import java.util.stream.Collectors;

@Service
public class MongoDbPerformanceStorageService implements PerformanceStorageService {

    private static final Logger LOGGER = LoggerFactory.getLogger(MongoDbPerformanceStorageService.class);

    @Qualifier("performanceDatastore")
    @Autowired
    private Datastore datastore;

    @Override
    public void save(Set<String> externalMatchIds, List<PerformanceResult> results) {
        LOGGER.debug("Saving {}", results.stream().filter( r -> r.getExternalId() != null).map(PerformanceResult::getExternalId).collect(Collectors.toList()));
        retireAll(externalMatchIds);
        datastore.save(results);
    }

    @Override
    public List<PerformanceResult> findActive(LocalDateTime start, LocalDateTime end) {
        Query<PerformanceResult> query = datastore.find(PerformanceResult.class);
        query.and(
                query.criteria("active").equal(true),
                query.criteria("betImportTime").greaterThanOrEq(start),
                query.criteria("betImportTime").lessThanOrEq(end));
        return query.asList();
    }

    @Override
    public List<PerformanceResult> findAllActive() {

        return datastore.find(PerformanceResult.class)
                .filter("active", true)
                .order("-betImportTime")
                .asList();
    }

    private void retireAll() {
        Query<PerformanceResult> query = datastore.createQuery(PerformanceResult.class)
                .filter("active", true)
                .filter("persistent", false);
        UpdateOperations<PerformanceResult> ops = datastore
                .createUpdateOperations(PerformanceResult.class)
                .set("active", false);
        UpdateResults update = datastore.update(query, ops);
        LOGGER.info("Retired: " + update.getUpdatedCount());
    }

    private void retireAll(Set<String> externalMatchIds) {
        Query<PerformanceResult> query = datastore.createQuery(PerformanceResult.class)
                .filter("active", true)
                .field("externalMatchId").in(externalMatchIds);
        WriteResult delete = datastore.delete(query);
        LOGGER.info("Deleted: " + delete);
    }


}
