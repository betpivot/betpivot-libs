package com.betting.storage.service.performance;

import com.betting.storage.data.performance.PerformanceResult;

import java.time.LocalDateTime;
import java.util.List;
import java.util.Set;

public interface PerformanceStorageService {
    void save(Set<String> externalMatchIds, List<PerformanceResult> periods);
    List<PerformanceResult>  findActive(LocalDateTime start, LocalDateTime end);
    List<PerformanceResult>  findAllActive();
}
