package com.betting.storage.service.performance;

import com.betting.storage.data.performance.*;
import com.betting.storage.data.result.PredictionResult;
import com.betting.storage.data.suggestion.*;
import com.betting.storage.service.configuration.ConfigurationStorageService;
import com.betting.storage.service.result.ResultStorageService;
import com.betting.storage.service.suggestion.MongoDbSuggestionStorageService;
import com.betting.storage.service.suggestion.SuggestionStorageService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.time.LocalDateTime;
import java.time.ZoneOffset;
import java.time.ZonedDateTime;
import java.time.temporal.ChronoUnit;
import java.util.*;
import java.util.stream.Collectors;
import java.util.stream.Stream;

@Service
public class PerformanceIntegrationServiceImpl implements PerformanceIntegrationService {

    private static final Logger LOGGER = LoggerFactory.getLogger(MongoDbSuggestionStorageService.class);
    public static final int RESULTS_MAX_SIZE = 200;

    private final ConfigurationStorageService configurationStorage;
    private final SuggestionStorageService suggestionStorage;
    private final PerformanceStorageService performanceStorage;
    private final ResultStorageService resultStorage;

    @Autowired
    public PerformanceIntegrationServiceImpl(ConfigurationStorageService configurationStorage, SuggestionStorageService suggestionStorage, PerformanceStorageService performanceStorage, ResultStorageService resultStorage) {
        this.configurationStorage = configurationStorage;
        this.suggestionStorage = suggestionStorage;
        this.performanceStorage = performanceStorage;
        this.resultStorage = resultStorage;
    }

    @Override
    public void recalculatePerformance(Map<String, List<PredictionResult>> results) {
        LOGGER.info("Calculating performance");
        Comparator<PerformanceResult> rateComp = Comparator.comparing(PerformanceResult::getRate);
        Comparator<PerformanceResult> resultValComp = Comparator.comparing(PerformanceResult::getResultValue);
        List<PerformanceResult> performanceResults = suggestionStorage
                .getEvaluated(results.keySet()).stream()
                .flatMap(m -> processOneMatch(m, results.get(m.getExternalMatchId())))
                .collect(Collectors.groupingBy(PerformanceResult::getExternalId))
                .values().stream().map(rl -> rl.stream().min(resultValComp.thenComparing(rateComp.reversed())))
                .filter(Optional::isPresent).map(Optional::get).collect(Collectors.toList());
        performanceStorage.save(results.keySet(), performanceResults);
    }

    private Stream<PerformanceResult> processOneMatch(SuggestionMatch m, List<PredictionResult> predictionResults) {
        return Stream
                .concat(m.getTeam1().getPlayers().stream(), m.getTeam2().getPlayers().stream()).flatMap(p -> {
                    PredictionResult result = predictionResults.stream().filter(r -> r.getExternalId().equals(p.getExternalId())).findFirst().orElse(null);
                    return processOnePlayer(result, m, p);
                });
    }

    @Override
    public void importPerformance(List<PerformanceResult> results) {
        LOGGER.info("Importing performance\n{}", results);
        results.forEach(r -> r.setPersistent(true));
        performanceStorage.save(Collections.emptySet(), results);
    }

    private Stream<PerformanceResult> processOnePlayer(PredictionResult result, SuggestionMatch match, SuggestionPlayer suggestionPlayer) {
        return suggestionPlayer.getSuggestions().stream()
                .map(s -> processOneSuggestion(result, match, suggestionPlayer, s))
                .filter(Optional::isPresent).map(Optional::get);
    }

    private Optional<PerformanceResult> processOneSuggestion(PredictionResult result, SuggestionMatch match, SuggestionPlayer suggestionPlayer, Suggestion suggestion) {
        String playerName = suggestionPlayer.getPlayerName();
        String providerId = suggestion.getProviderId();
        if (!SuggestionState.PROFITABLE.equals(suggestion.getState())) {
            return Optional.empty();
        }
        SuggestionType type = SuggestionType.valueOf(suggestion.getType().name());
        LocalDateTime betImportTime = match.getImportTime();
        String teamNameId = match.belongsToTeam(suggestionPlayer).map(SuggestionTeam::getNameId).orElse(null);
        String externalId = suggestionPlayer.getExternalId();
        String externalMatchId = match.getExternalMatchId();
        if (result == null) {
            PerformanceResult value = new PerformanceResult(playerName, teamNameId, providerId, type, suggestion.getDontBetUnder(), suggestion.getRate(), suggestion.getLine(), null, betImportTime, null, PerformanceResultValue.NOT_EVALUATED, suggestion.getProbability(), externalId, externalMatchId);
            LOGGER.warn("Suggestion not evaluated resulting {}", value);
            return Optional.of(value);
        }
        Integer points = result.getPoints();
        LocalDateTime resultImportTime = result.getImportTime();
        PerformanceResultValue resultValue;
        if (!result.getPlayed()) {
            resultValue = PerformanceResultValue.NOT_PLAYED;
        } else if (SuggestionType.LESS.equals(type) && points <= suggestion.getLine()) {
            resultValue = PerformanceResultValue.WINNING;
        } else if (SuggestionType.MORE.equals(type) && points >= suggestion.getLine()) {
            resultValue = PerformanceResultValue.WINNING;
        } else {
            resultValue = PerformanceResultValue.NOT_WINNING;
        }

        PerformanceResult value = new PerformanceResult(playerName, teamNameId, providerId, type, suggestion.getDontBetUnder(), suggestion.getRate(), suggestion.getLine(), points, betImportTime, resultImportTime, resultValue, suggestion.getProbability(), externalId, externalMatchId);
        //LOGGER.trace("PerformanceResult {}", value);
        return Optional.of(value);
    }
}
