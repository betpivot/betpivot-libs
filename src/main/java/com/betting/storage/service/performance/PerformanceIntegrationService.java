package com.betting.storage.service.performance;

import com.betting.storage.data.performance.PerformanceResult;
import com.betting.storage.data.result.PredictionResult;

import java.util.List;
import java.util.Map;

public interface PerformanceIntegrationService {
    void recalculatePerformance(Map<String, List<PredictionResult>> results);

    void importPerformance(List<PerformanceResult> results);
}
