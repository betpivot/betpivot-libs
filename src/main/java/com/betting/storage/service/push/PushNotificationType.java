package com.betting.storage.service.push;

import lombok.AllArgsConstructor;
import lombok.Getter;

@AllArgsConstructor
public enum PushNotificationType {
    PREDICTION_NEW("prediction.new"),
    BETS_FIRST_LOAD("bet.new"),
    RESULTS_NEW("result.new");

    private final String topic;

    public String getTopic() {
        return "cz." + topic;
    }

    public String getTopicWithSuffix(String suffix) {
        return "cz." + topic + "." + suffix;
    }
}
