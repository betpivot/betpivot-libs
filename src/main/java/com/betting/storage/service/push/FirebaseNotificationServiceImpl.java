package com.betting.storage.service.push;

import com.betting.storage.data.betting.BettingProvider;
import com.betting.storage.util.HashingService;
import com.google.firebase.messaging.*;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.time.Instant;

@Service
public class FirebaseNotificationServiceImpl implements PushNotificationService {

    private static final Logger LOGGER = LoggerFactory.getLogger(FirebaseNotificationServiceImpl.class);

    private final HashingService hashingService;

    @Autowired
    public FirebaseNotificationServiceImpl(HashingService hashingService) {
        this.hashingService = hashingService;
    }

    @Override
    public void notifyNewPredictions() {
        Message message = getMessageBuilder("Dokončili jsme výpočet predikcí", "Podívejte se na předpovězené počty bodů hráčů.", PushNotificationType.PREDICTION_NEW.name())
                .setTopic(PushNotificationType.PREDICTION_NEW.getTopic())
                .build();
        sendMessage(PushNotificationType.PREDICTION_NEW, message);
    }

    @Override
    public void notifyNewBets(BettingProvider bp) {
            String providerId = hashingService.hash(bp.getId().toHexString());
        String title = String.format("Jsou dostupné nové tipy u %s", bp.getNotifyName());
        String body = String.format("Podívejte se, které tipy stojí za to vsadit.", bp.getName());
        Message message = getMessageBuilder(title, body, PushNotificationType.BETS_FIRST_LOAD.name())
                    .setTopic(PushNotificationType.BETS_FIRST_LOAD.getTopicWithSuffix(providerId))
                    .build();
         sendMessage(PushNotificationType.BETS_FIRST_LOAD, message);
    }

    @Override
    public void notifyNewResults() {
        Message message = getMessageBuilder("Aktualizovali jsme výsledky", "V aplikaci si můžete zkontrolovat, jakou úspěšnost měly tipy z předchozího dne.", PushNotificationType.RESULTS_NEW.name())
                .setTopic(PushNotificationType.RESULTS_NEW.getTopic())
                .build();
        sendMessage(PushNotificationType.RESULTS_NEW, message);
    }

    private void sendMessage(PushNotificationType type, Message message) {
        new Thread(() -> {
            try {
                Thread.sleep(10000);
                try {
                    String response = FirebaseMessaging.getInstance().send(message);
                    LOGGER.info("Sent push notification of type {} to topic {} with data {} and response {}", type, type.getTopic(), message, response);
                } catch (FirebaseMessagingException e) {
                    LOGGER.error("Failed to send push notification: {}", message.toString(), e);
                }
            } catch (InterruptedException e) {
                LOGGER.error("Failed to send push notification: {}", message.toString(), e);
                throw new RuntimeException(e);
            }
        }).start();
    }

    private Message.Builder getMessageBuilder(String title, String body, String type) {
        LOGGER.info("Sending notification (title={}, body={}, type={})");
        long timeToLiveSeconds = 5 * 60 * 60;
        long timeToLiveMillis = timeToLiveSeconds * 1000;
        long expirationStampSeconds = Instant.now().plusSeconds(timeToLiveSeconds).getEpochSecond();

        ApnsConfig appleNotification = ApnsConfig.builder()
                .setAps(Aps.builder()
                        .setAlert(ApsAlert.builder()
                                .setTitle(title)
                                .setBody(body).build())
                        .putCustomData("type", type).build())
                .putHeader("apns-expiration", String.valueOf(expirationStampSeconds)).build();

        AndroidConfig androidNotification = AndroidConfig.builder()
                .setNotification(AndroidNotification.builder()
                        .setTitle(title)
                        .setBody(body).build())
                .putData("type", type)
                .setPriority(AndroidConfig.Priority.HIGH)
                .setTtl(timeToLiveMillis).build();

        WebpushConfig webNotification = WebpushConfig.builder()
                .putHeader("TTL", String.valueOf(timeToLiveSeconds))
                .setNotification(WebpushNotification.builder()
                        .setBody(body)
                        .setTitle(title).build())
                .putData("type", type).build();

        return Message.builder()
                .setApnsConfig(appleNotification)
                .setAndroidConfig(androidNotification)
                .setWebpushConfig(webNotification)
                .setNotification(new Notification(title, body))
                .putData("type", type);

    }
}
