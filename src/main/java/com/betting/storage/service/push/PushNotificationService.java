package com.betting.storage.service.push;

import com.betting.storage.data.betting.BettingProvider;
import com.betting.storage.data.betting.BettingProviderMatch;
import com.betting.storage.data.prediction.PredictionMatch;

import java.util.List;

public interface PushNotificationService {

    void notifyNewPredictions();
    void notifyNewBets(BettingProvider bp);
    void notifyNewResults();
}
