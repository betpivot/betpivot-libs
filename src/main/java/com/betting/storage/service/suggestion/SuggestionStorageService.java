package com.betting.storage.service.suggestion;

import com.betting.storage.data.suggestion.SuggestionMatch;
import com.betting.storage.data.suggestion.SuggestionMatchState;

import java.time.LocalDateTime;
import java.util.List;
import java.util.Optional;
import java.util.Set;

public interface SuggestionStorageService {

    void saveSuggestions(List<SuggestionMatch> data);
    List<SuggestionMatch> getActiveMatches();
    long evaluateAll(Set<String> externalMatchIds);

    List<SuggestionMatch> getEvaluated(Set<String> externalMatchIds);

    long delete(LocalDateTime olderThen);
}
