package com.betting.storage.service.suggestion;

import com.betting.storage.data.suggestion.SuggestionMatch;

import java.util.List;

public interface SuggestionIntegrationService {
    List<SuggestionMatch> calculateSuggestions();
}
