package com.betting.storage.service.suggestion;

import com.betting.storage.data.betting.BettingProvider;
import com.betting.storage.data.betting.BettingProviderMatch;
import com.betting.storage.data.messaging.BettingMessage;
import com.betting.storage.data.messaging.BettingMessageType;
import com.betting.storage.data.prediction.Prediction;
import com.betting.storage.data.prediction.PredictionMatch;
import com.betting.storage.data.suggestion.*;
import com.betting.storage.fail.BusinessFail;
import com.betting.storage.monitoring.Monitoring;
import com.betting.storage.service.betting.BettingProviderStorageService;
import com.betting.storage.service.messaging.MessagingStorageService;
import com.betting.storage.service.prediction.PredictionStorageService;
import com.betting.storage.util.HashingService;
import com.betting.storage.util.StatisticalService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Comparator;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.stream.Collectors;

@Service
public class SuggestionIntegrationServiceImpl implements SuggestionIntegrationService {

    private static final Logger LOGGER = LoggerFactory.getLogger(SuggestionIntegrationServiceImpl.class);

    private final PredictionStorageService predictionStorage;
    private final SuggestionStorageService suggestionStorage;
    private final BettingProviderStorageService bettingStorage;
    private final HashingService hashingService;
    private final StatisticalService statisticalService;
    private final MessagingStorageService messagingStorage;

    @Autowired
    public SuggestionIntegrationServiceImpl(PredictionStorageService predictionStorage, SuggestionStorageService suggestionStorage, BettingProviderStorageService bettingStorage, HashingService hashingService, StatisticalService statisticalService, MessagingStorageService messagingStorage) {
        this.predictionStorage = predictionStorage;
        this.suggestionStorage = suggestionStorage;
        this.bettingStorage = bettingStorage;
        this.hashingService = hashingService;
        this.statisticalService = statisticalService;
        this.messagingStorage = messagingStorage;
    }

    @Override
    public synchronized List<SuggestionMatch> calculateSuggestions() {
        LOGGER.info("Recalculating suggestions");
        List<BettingProvider> activeProviders = bettingStorage.getActiveProviders();
        List<SuggestionMatch> matches = predictionStorage.getActivePredictions().stream()
                .map(predictionMatch -> processOnePredictionMatch(predictionMatch, activeProviders))
                .filter(Optional::isPresent)
                .map(Optional::get).collect(Collectors.toList());
        LOGGER.info("Resulting matches: {}", matches);
        suggestionStorage.saveSuggestions(matches);
        messagingStorage.postMessage(new BettingMessage(BettingMessageType.NEW_SUGGESTION, "suggestions calculated"));

        return matches;
    }

    private Optional<SuggestionMatch> processOnePredictionMatch(PredictionMatch predictionMatch, List<BettingProvider> activeProviders) {
        String team1NameId = predictionMatch.getTeam1().getNameId();
        String team2NameId = predictionMatch.getTeam2().getNameId();
        List<BettingProviderMatch> matches = bettingStorage.getMatches(predictionMatch.getExternalMatchId());
        Map<BettingProvider, List<BettingProviderMatch>> providers = activeProviders.stream()
                .collect(Collectors.toMap(bp -> bp, bp -> matches.stream()
                                        .filter(m -> m.getProvider().equals(bp)).collect(Collectors.toList())));
        List<SuggestionPlayer> restPlayers1 = predictionMatch.getTeam1Predictions().stream()
                .map(predictionSuggestion -> processOnePlayer(providers, predictionSuggestion))
                .collect(Collectors.toList());
        List<SuggestionPlayer> restPlayers2 = predictionMatch.getTeam2Predictions().stream()
                .map(predictionSuggestion -> processOnePlayer(providers, predictionSuggestion))
                .collect(Collectors.toList());

        SuggestionTeam team1 = new SuggestionTeam(team1NameId, restPlayers1);
        SuggestionTeam team2 = new SuggestionTeam(team2NameId, restPlayers2);
        return Optional.of(new SuggestionMatch(predictionMatch.getExternalMatchId(), predictionMatch.getStartTime(), team1, team2));
    }

    private SuggestionPlayer processOnePlayer(Map<BettingProvider, List<BettingProviderMatch>> providers, Prediction prediction) {
        Comparator<Suggestion> stateComparator = Comparator.comparingInt(b -> b.getState().order);
        Comparator<Suggestion> valueComparator = Comparator.comparing(Suggestion::getProbability, Comparator.nullsFirst(Comparator.naturalOrder()));
        List<Suggestion> suggestions = providers.entrySet().stream()
                .map(bettingProvider -> processOneBettingProvider(prediction, bettingProvider))
                .sorted(stateComparator.thenComparing(valueComparator.reversed()))
                .collect(Collectors.toList());

        String suggestionToPredictionId = prediction.getExternalId();
        return new SuggestionPlayer(prediction.getPlayer().getFullName(), suggestionToPredictionId, suggestions);
    }

    private Suggestion processOneBettingProvider(Prediction prediction, Map.Entry<BettingProvider, List<BettingProviderMatch>> bettingProvider) {
        String providerId = hashingService.hash(bettingProvider.getKey().getId().toHexString());
        if (bettingProvider.getValue().size() < 1) {
            return Suggestion.ofWaitingForProvider(providerId);
        }
        if (bettingProvider.getValue().size() > 1) {
            throw BusinessFail.MORE_ACTIVE_BETTING_MATCHES_FOR_ONE_PROVIDER
                    .exception(Monitoring.BusinessObject.BET, "Cannot calculate suggestion", bettingProvider.getKey().getName(), getClass());
        }
        return processOneBet(prediction, providerId, bettingProvider.getValue().get(0));
    }

    private Suggestion processOneBet(Prediction prediction, String providerId, BettingProviderMatch providerMatch) {
        Comparator<Suggestion> stateComparator = Comparator.comparingInt(b -> b.getState().order);
        Comparator<Suggestion> valueComparator = Comparator.comparing(Suggestion::getProbability, Comparator.nullsFirst(Comparator.naturalOrder()));
        return providerMatch.getByExternalId(prediction.getExternalId()).stream()
                .map(bet -> {
                    if (prediction.getProbability() < 0.9) {
                        return Suggestion.ofWillNotPlay(providerId);
                    }
                    if (bet.getLine() != null && bet.getLine() <= 3.5) {
                        return Suggestion.ofWillNotPlay(providerId);
                    }
                    if (bet.getLine() != null && bet.getLine() >= 34.5) {
                        return Suggestion.ofWillNotPlay(providerId);
                    }
                    if (bet.getRate() != null && (bet.getRate() >= 5.0 || bet.getRate() <= 1.2)) {
                        return Suggestion.ofWillNotPlay(providerId);
                    }
                    Double probability = statisticalService.getProbability(prediction, bet);
                    if (bet.getType().more()) {
                        double moreProb = probability * bet.getRate();
                        double dontBetUnder = 1.0 / probability;
                        if (moreProb > 1.07) {
                            if (dontBetUnder < 1.1) {
                                BusinessFail.DONT_BET_UNDER_TOO_LOW.report(Monitoring.BusinessObject.BET, "Replacing with error suggestion", bet.toString(), getClass());
                                return Suggestion.ofParsingFailed(providerId);
                            }
                            dontBetUnder = dontBetUnder * 1.07;
                            return Suggestion.ofProfitable(providerId, SuggestionType.MORE, dontBetUnder, bet.getRate(), bet.getLine(), bet.getBetLinkUrl(), bet.getBetDeepLink(), moreProb);
                        }
                        return Suggestion.ofNotProfitable(providerId, SuggestionType.MORE, dontBetUnder, bet.getRate(), bet.getLine(), bet.getBetLinkUrl(), bet.getBetDeepLink(), moreProb);
                    } else {
                        double lessProb = (1 - probability) * bet.getRate();
                        double dontBetUnder = 1.0 / (1 - probability);
                        if (lessProb > 1.07) {
                            if (dontBetUnder < 1.1) {
                                BusinessFail.DONT_BET_UNDER_TOO_LOW.report(Monitoring.BusinessObject.BET, "Replacing with error suggestion", bet.toString(), getClass());
                                return Suggestion.ofParsingFailed(providerId);
                            }
                            dontBetUnder = dontBetUnder * 1.07;
                            return Suggestion.ofProfitable(providerId, SuggestionType.LESS, dontBetUnder, bet.getRate(), bet.getLine(), bet.getBetLinkUrl(), bet.getBetDeepLink(), lessProb);
                        }
                        return Suggestion.ofNotProfitable(providerId, SuggestionType.LESS, dontBetUnder, bet.getRate(), bet.getLine(), bet.getBetLinkUrl(), bet.getBetDeepLink(), lessProb);
                    }
                }).min(stateComparator.thenComparing(valueComparator.reversed()))
                .orElse(Suggestion.ofMissingInProvider(providerId));
    }
}
