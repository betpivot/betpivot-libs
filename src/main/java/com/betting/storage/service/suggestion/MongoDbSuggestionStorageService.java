package com.betting.storage.service.suggestion;

import com.betting.storage.data.suggestion.SuggestionMatch;
import com.betting.storage.data.suggestion.SuggestionMatchState;
import com.betting.storage.data.suggestion.SuggestionPlayer;
import org.mongodb.morphia.Datastore;
import org.mongodb.morphia.query.Query;
import org.mongodb.morphia.query.UpdateOperations;
import org.mongodb.morphia.query.UpdateResults;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.context.annotation.Primary;
import org.springframework.stereotype.Service;

import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.Set;
import java.util.stream.Collectors;

@Service
@Primary
public class MongoDbSuggestionStorageService implements SuggestionStorageService {

    private static final Logger LOGGER = LoggerFactory.getLogger(MongoDbSuggestionStorageService.class);

    @Qualifier("suggestionDatastore")
    @Autowired
    private Datastore datastore;


    @Override
    public void saveSuggestions(List<SuggestionMatch> data) {
        LOGGER.info("Saving: {}", data.size());
        retireAllMatches();
        datastore.save(data);
    }

    @Override
    public List<SuggestionMatch> getActiveMatches() {
        return datastore.find(SuggestionMatch.class).filter("state", SuggestionMatchState.ACTIVE).asList();
    }

    @Override
    public long evaluateAll(Set<String> externalMatchIds) {
        Query<SuggestionMatch> q = datastore.createQuery(SuggestionMatch.class);
        q.and(
                q.or(
                        q.criteria("state").equal(SuggestionMatchState.ACTIVE),
                        q.criteria("state").equal(SuggestionMatchState.RETIRED)),
                q.criteria("externalMatchId").in(externalMatchIds));

        UpdateOperations<SuggestionMatch> update = datastore
                .createUpdateOperations(SuggestionMatch.class).set("state", SuggestionMatchState.EVALUATED);
        return datastore.update(q, update).getUpdatedCount();
    }

    @Override
    public List<SuggestionMatch> getEvaluated(Set<String> externalMatchIds) {
        Query<SuggestionMatch> q = datastore.createQuery(SuggestionMatch.class);
        q.and(
                q.criteria("state").equal(SuggestionMatchState.EVALUATED),
                q.criteria("externalMatchId").in(externalMatchIds)
        );

        return q.asList();
    }

    @Override
    public long delete(LocalDateTime olderThen) {
        Query<SuggestionMatch> q = datastore
                .createQuery(SuggestionMatch.class)
                .field("importTime").lessThanOrEq(olderThen);


        return datastore.delete(q).getN();
    }

    private void retireAllMatches() {
        Query<SuggestionMatch> query = datastore.createQuery(SuggestionMatch.class)
                .filter("state", SuggestionMatchState.ACTIVE);
        UpdateOperations<SuggestionMatch> ops = datastore
                .createUpdateOperations(SuggestionMatch.class)
                .set("state", SuggestionMatchState.RETIRED);
        UpdateResults update = datastore.update(query, ops);
        LOGGER.info("Retired: " + update.getUpdatedCount());
    }
}
