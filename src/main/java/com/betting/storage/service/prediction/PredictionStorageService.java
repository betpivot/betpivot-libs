package com.betting.storage.service.prediction;

import com.betting.storage.data.prediction.PredictionMatch;
import com.betting.storage.data.prediction.PredictionTeam;

import java.util.List;
import java.util.Optional;
import java.util.Set;

public interface PredictionStorageService {
    long saveMatches(List<PredictionMatch> toDbData);
    Optional<PredictionTeam> getTeam(String nameId);
    List<PredictionMatch> getActivePredictions();
    List<PredictionTeam> getTeams();
    long evaluateAll(Set<String> externalMatchIds);

    Optional<PredictionTeam>  getTeamAny(String name);
}
