package com.betting.storage.service.prediction;

import com.betting.storage.data.prediction.PredictionMatch;
import com.betting.storage.data.prediction.PredictionMatchState;
import com.betting.storage.data.prediction.PredictionTeam;
import org.mongodb.morphia.Datastore;
import org.mongodb.morphia.query.Query;
import org.mongodb.morphia.query.UpdateOperations;
import org.mongodb.morphia.query.UpdateResults;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;
import java.util.Set;
import java.util.stream.Collectors;

@Service
public class MongoDbPredictionStorageService implements PredictionStorageService {

    private static final Logger LOGGER = LoggerFactory.getLogger(MongoDbPredictionStorageService.class);

    @Qualifier("predictionDatastore")
    @Autowired
    private Datastore datastore;

    @Override
    public long saveMatches(List<PredictionMatch> data) {
        LOGGER.info("Saving: {}", data.size());
        long retired = retireAllMatches(data);
        datastore.save(data);
        return retired;
    }

    @Override
    public Optional<PredictionTeam> getTeam(String nameId) {
        return Optional.ofNullable(datastore.find(PredictionTeam.class).filter("nameId", nameId).get());
    }

    @Override
    public List<PredictionMatch> getActivePredictions() {
        Query<PredictionMatch> query = datastore.find(PredictionMatch.class);
        query.criteria("state").equal(PredictionMatchState.ACTIVE);
        return query.asList();
    }

    @Override
    public List<PredictionTeam> getTeams() {
        return datastore.find(PredictionTeam.class).asList();
    }

    @Override
    public long evaluateAll(Set<String> externalMatchIds) {
        Query<PredictionMatch> q = datastore.find(PredictionMatch.class);
        q.and(
                q.or(
                    q.criteria("state").equal(PredictionMatchState.ACTIVE),
                    q.criteria("state").equal(PredictionMatchState.RETIRED)
                ),
                q.criteria("externalMatchId").in(externalMatchIds)
        );

        UpdateOperations<PredictionMatch> ops = datastore
                .createUpdateOperations(PredictionMatch.class)
                .set("state", PredictionMatchState.EVALUATED);
        UpdateResults update = datastore.update(q, ops);
        LOGGER.info("Retired " + update.getUpdatedCount());

        return update.getUpdatedCount();
    }

    private long retireAllMatches(List<PredictionMatch> data) {
        Query<PredictionMatch> query = datastore.createQuery(PredictionMatch.class)
                .filter("state", PredictionMatchState.ACTIVE)
                .field("externalMatchId").in(data.stream().map(PredictionMatch::getExternalMatchId).collect(Collectors.toList()));
        UpdateOperations<PredictionMatch> ops = datastore
                .createUpdateOperations(PredictionMatch.class)
                .set("state", PredictionMatchState.RETIRED);
        UpdateResults update = datastore.update(query, ops);
        LOGGER.info("Retired " + update.getUpdatedCount());

        return update.getUpdatedCount();
    }

    @Override
    public Optional<PredictionTeam> getTeamAny(String name) {
        return Optional.ofNullable(datastore.find(PredictionTeam.class).field("names").hasThisOne(name).get());
    }

    private long retireAllMatches() {
        Query<PredictionMatch> query = datastore.createQuery(PredictionMatch.class)
                .filter("state", PredictionMatchState.ACTIVE);
        UpdateOperations<PredictionMatch> ops = datastore
                .createUpdateOperations(PredictionMatch.class)
                .set("state", PredictionMatchState.RETIRED);
        UpdateResults update = datastore.update(query, ops);
        LOGGER.info("Retired " + update.getUpdatedCount());

        return update.getUpdatedCount();
    }
}
