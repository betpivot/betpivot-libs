package com.betting.storage.monitoring;

import org.slf4j.LoggerFactory;
import org.slf4j.Marker;
import org.slf4j.MarkerFactory;

public class Monitoring {

    public static void businessError(Monitoring.BusinessObject object, String message, String eventId, String instanceId, Class<?> cls) {
        Marker marker = MarkerFactory.getDetachedMarker("business");
        marker.add(MarkerFactory.getMarker(object.name()));
        marker.add(MarkerFactory.getMarker(eventId));
        marker.add(MarkerFactory.getMarker(instanceId));
        LoggerFactory.getLogger(cls).error(marker, message);
    }

    public static void businessError(Monitoring.BusinessObject object, String message, String eventId, String instanceId, Class<?> cls, Exception e) {
        Marker marker = MarkerFactory.getDetachedMarker("business");
        marker.add(MarkerFactory.getMarker(object.name()));
        marker.add(MarkerFactory.getMarker(eventId));
        marker.add(MarkerFactory.getMarker(instanceId));
        LoggerFactory.getLogger(cls).error(marker, message, e);
    }

    public static void serviceError(String message, String eventId, String instanceId, Class<?> cls, Throwable e) {
        Marker marker = MarkerFactory.getDetachedMarker("service");
        marker.add(MarkerFactory.getMarker(eventId));
        marker.add(MarkerFactory.getMarker(instanceId));
        LoggerFactory.getLogger(cls).error(marker, message, e);
    }

    public static void serviceError(String message, String eventId, String instanceId, Class<?> cls) {
        Marker marker = MarkerFactory.getDetachedMarker("service");
        marker.add(MarkerFactory.getMarker(eventId));
        marker.add(MarkerFactory.getMarker(instanceId));
        LoggerFactory.getLogger(cls).error(marker, message);
    }

    public static void serviceWarning(String message, String eventId, String instanceId, Class<?> cls, Throwable e) {
        Marker marker = MarkerFactory.getDetachedMarker("service");
        marker.add(MarkerFactory.getMarker(eventId));
        marker.add(MarkerFactory.getMarker(instanceId));
        LoggerFactory.getLogger(cls).warn(marker, message, e);
    }

    public static void serviceWarning(String message, String eventId, String instanceId, Class<?> cls) {
        Marker marker = MarkerFactory.getDetachedMarker("service");
        if (eventId != null) {
            marker.add(MarkerFactory.getMarker(eventId));
        }
        if (instanceId != null) {
            marker.add(MarkerFactory.getMarker(instanceId));
        }
        LoggerFactory.getLogger(cls).warn(marker, message);
    }

    public static void serviceInfo(String message, String eventId, String instanceId, Class<?> cls) {
        Marker marker = MarkerFactory.getDetachedMarker("service");
        if (eventId != null) {
            marker.add(MarkerFactory.getMarker(eventId));
        }
        if (instanceId != null) {
            marker.add(MarkerFactory.getMarker(instanceId));
        }
        LoggerFactory.getLogger(cls).info(marker, message);
    }

    public static void serviceDebug(String message, String eventId, String instanceId, Class<?> cls) {
        Marker marker = MarkerFactory.getDetachedMarker("service");
        marker.add(MarkerFactory.getMarker(eventId));
        marker.add(MarkerFactory.getMarker(instanceId));
        LoggerFactory.getLogger(cls).debug(marker, message);
    }

    public enum BusinessObject {
        PREDICTION,
        RESULT,
        BET,
        INIT;
    }
}
