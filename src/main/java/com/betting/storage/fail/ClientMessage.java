package com.betting.storage.fail;

public enum ClientMessage{

    WRONG_PROMO_CODE(1, "WRONG_PROMO_CODE"),
    CLIENT_ALREADY_ACTIVATED(2, "CLIENT_ALREADY_ACTIVATED"),
    TOO_MANY_ACTIVATION_ATTEMPTS(3, "TOO_MANY_ACTIVATION_ATTEMPTS"),
    CANNOT_ACTIVATE_OWN_PROMO_CODE(4, "CANNOT_ACTIVATE_OWN_PROMO_CODE"),
    CANNOT_CREATE_BILATERAL_RELATIONSHIP_BETWEEN_PROMO_CODES(5, "CANNOT_CREATE_BILATERAL_RELATIONSHIP_BETWEEN_PROMO_CODES");

    private final int code;
    private final String message;

    ClientMessage(int code, String message) {
        this.code = code;
        this.message = message;
    }



    public FailException exception(String detailMessage) {
        return new FailException(Severity.VALIDATION, code, message, detailMessage);
    }

    public FailException exception(String detailMessage, Exception e) {
        return new FailException(Severity.VALIDATION, code, message, detailMessage, e);
    }
}
