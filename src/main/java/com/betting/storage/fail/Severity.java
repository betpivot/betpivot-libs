package com.betting.storage.fail;

public enum Severity {
    INTERNAL_ERROR,
    VALIDATION;
}
