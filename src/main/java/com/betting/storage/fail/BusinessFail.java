package com.betting.storage.fail;

import com.betting.storage.monitoring.Monitoring;
import lombok.Getter;

@Getter
public enum BusinessFail {

    PROVIDER_NOT_FOUND(1, "api.provider.notFound", "Provider was not found"),
    ANOTHER_RUNNING_PREDICTION_MATCH(2, "api.prediction.import.fail.duplicity", "Duplicate prediction with different start time."),
    PREDICTION_IMPORT_VALIDATION(3, "api.prediction.import.fail", "Failed prediction import validation."),
    CONFIGURATION_MISSING(4, "api.init.configuration.missing", "Configuration not found."),
    TEAM_NOT_FOUND(5, "api.prediction.team.notFound", "Team not found"),
    MORE_ACTIVE_BETTING_MATCHES_FOR_ONE_PROVIDER(6, "platform.suggestion.calculate.fail.moreActiveMatchesForOneProvider", "There are more then one active matches for one betting provider"),
    DONT_BET_UNDER_TOO_LOW(7, "platform.suggestion.calculate.incosistency", "Don't bed under value is too low. Possible wrong pairing.");

    private final int code;
    private final String eventId;
    private final String message;

    BusinessFail(int code, String eventId, String message) {
        this.code = code;
        this.eventId = eventId;
        this.message = message;
    }

    public FailException exception(Monitoring.BusinessObject object, String detailMessage, String instanceId, Class<?> cls) {
        Monitoring.businessError(object, message + ": " + detailMessage, eventId, instanceId, cls);
        return new FailException(Severity.INTERNAL_ERROR, code, message, "[" + instanceId + "] " + detailMessage);
    }

    public FailException exception(Monitoring.BusinessObject object, String detailMessage, String instanceId, Class<?> cls, Exception e) {
        Monitoring.businessError(object, message + ": " + detailMessage, eventId, instanceId, cls);
        return new FailException(Severity.INTERNAL_ERROR, code, message, "[" + instanceId + "] " + detailMessage, e);
    }

    public void report(Monitoring.BusinessObject object, String detailMessage, String instanceId, Class<?> cls) {
        Monitoring.businessError(object, message + ": " + "[" + instanceId + "] " + detailMessage, eventId, instanceId, cls);
    }
}
