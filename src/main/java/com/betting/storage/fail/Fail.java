package com.betting.storage.fail;

public interface Fail {
    int code();
    String message();
    String detailMessage();
    Severity severity();
}
