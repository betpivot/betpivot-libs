package com.betting.storage.fail;

public class FailException extends RuntimeException implements Fail {

    private final int code;
    private final String message;
    private final String detailMessage;
    private final Severity severity;

    public FailException(Severity severity, int code, String message, String detailMessage) {
        super(message);
        this.code = code;
        this.message = message;
        this.detailMessage = detailMessage;
        this.severity = severity;
    }

    public FailException(Severity severity, int code, String message, String detailMessage, Throwable cause) {
        super(message, cause);
        this.code = code;
        this.message = message;
        this.detailMessage = detailMessage;
        this.severity = severity;
    }

    @Override
    public int code() {
        return code;
    }

    @Override
    public String message() {
        return message;
    }

    @Override
    public String detailMessage() {
        return detailMessage;
    }

    @Override
    public Severity severity() {
        return severity;
    }
}
