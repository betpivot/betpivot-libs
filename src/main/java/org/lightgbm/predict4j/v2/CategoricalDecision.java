package org.lightgbm.predict4j.v2;

/**
 * @author lyg5623
 */
public class CategoricalDecision<T extends Comparable<T>> extends Decision<T> {
    boolean decision(T fval, T threshold) {
        Object o1 = fval;
        Object o2 = threshold;
        if ((int) o1 == (int) o2) {
            return true;
        } else {
            return false;
        }
    }
}
