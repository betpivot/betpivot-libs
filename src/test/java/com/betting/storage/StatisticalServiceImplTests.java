package com.betting.storage;

import com.betting.storage.util.StatisticalServiceImpl;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import java.math.BigDecimal;
import java.math.RoundingMode;

import static org.assertj.core.api.Assertions.assertThat;
import static org.assertj.core.api.Assertions.within;

@RunWith(SpringRunner.class)
@SpringBootTest(classes = StatisticalServiceImpl.class)
public class StatisticalServiceImplTests {

    @Autowired
    private StatisticalServiceImpl statisticalService;

    @Test
    public void serviceNotNull() {
        assertThat(statisticalService).isNotNull();
    }

    @Test
    public void testNParam() {
        assertThat(statisticalService.getNParam(15.36).setScale(10, RoundingMode.HALF_UP))
                .isEqualTo(new BigDecimal(8.863769405139204).setScale(10, RoundingMode.HALF_UP));
        assertThat(statisticalService.getNParam(35.68).setScale(10, RoundingMode.HALF_UP))
                .isEqualTo(new BigDecimal(27.855514531291135).setScale(10, RoundingMode.HALF_UP));
        assertThat(statisticalService.getNParam(4.61).setScale(10, RoundingMode.HALF_UP))
                .isEqualTo(new BigDecimal(1.7464250703808522).setScale(10, RoundingMode.HALF_UP));
    }

    @Test
    public void testPParam() {
        assertThat(statisticalService.getPParam(15.36).setScale(10, RoundingMode.HALF_UP))
                .isEqualTo(new BigDecimal(0.3633397828727343).setScale(10, RoundingMode.HALF_UP));
        assertThat(statisticalService.getPParam(35.68).setScale(10, RoundingMode.HALF_UP))
                .isEqualTo(new BigDecimal(0.48412752247058277).setScale(10, RoundingMode.HALF_UP));
        assertThat(statisticalService.getPParam(4.61).setScale(10, RoundingMode.HALF_UP))
                .isEqualTo(new BigDecimal(0.2781365697220081).setScale(10, RoundingMode.HALF_UP));
    }

    @Test
    public void testProbability() {
        assertThat(statisticalService.getProbability(8.88, 0.35, 25.5))
                .isEqualTo(0.8972688674152307, within(0.0000000001));
        assertThat(statisticalService.getProbability(9.86, 0.36, 17.5))
                .isEqualTo(0.541890529659513, within(0.0000000001));
        assertThat(statisticalService.getProbability(10.5, 0.3, 22.5))
                .isEqualTo(0.45181705023106283, within(0.0000000001));
    }
}
