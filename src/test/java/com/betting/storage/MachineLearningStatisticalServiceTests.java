package com.betting.storage;

import com.betting.storage.data.betting.BettingProviderBet;
import com.betting.storage.data.prediction.Prediction;
import com.betting.storage.service.suggestion.SuggestionIntegrationService;
import com.betting.storage.util.MachineLearningStatisticalServiceImpl;
import com.betting.storage.util.StatisticalServiceImpl;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.ImportResource;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.context.support.AnnotationConfigContextLoader;

import java.math.BigDecimal;
import java.math.RoundingMode;

import static org.assertj.core.api.Assertions.assertThat;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(
        loader = AnnotationConfigContextLoader.class,
        classes = MachineLearningStatisticalServiceTests.Config.class)
public class MachineLearningStatisticalServiceTests {

    @Configuration
    @ComponentScan("com.betting.storage")
    public static class Config {

    }

    @Autowired
    private MachineLearningStatisticalServiceImpl statisticalService;

    @Autowired
    private SuggestionIntegrationService suggestionIntegrationService;

    @Test
    public void serviceNotNull() {
        assertThat(statisticalService).isNotNull();
    }

    @Test
    public void test1() {
        Prediction prediction = new Prediction(null, 0.989, 28.029, 10.804, null);
        BettingProviderBet bet = new BettingProviderBet(null, 11.5, null, null, null, null, null);
        double probability = statisticalService.getProbability(prediction, bet);


        assertThat(new BigDecimal(probability).setScale(6, RoundingMode.HALF_UP)).isEqualTo(new BigDecimal(0.418402).setScale(6, RoundingMode.HALF_UP));
    }

    @Test
    public void test2() {
        Prediction prediction = new Prediction(null, 0.979, 27.011, 20.9, null);
        BettingProviderBet bet = new BettingProviderBet(null, 19.5, null, null, null, null, null);
        double probability = statisticalService.getProbability(prediction, bet);


        assertThat(new BigDecimal(probability).setScale(6, RoundingMode.HALF_UP)).isEqualTo(new BigDecimal(0.553249).setScale(6, RoundingMode.HALF_UP));
    }
}
